from pathlib import Path


def get_project_root() -> Path:
    return Path(__file__).parent.parent.parent.resolve()


def get_total_problems() -> Path:
    return get_project_root() / "resources/_problems"


def get_readme_path() -> Path:
    return get_project_root() / "README.md"
