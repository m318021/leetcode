import os
import logging
from git import Repo

logging.basicConfig(level=logging.INFO)


def get_git_remote_branch() -> list:
    repo = Repo(".")
    remote_refs = repo.remote().refs
    branches = []
    for refs in remote_refs:
        branches.append(refs.name)

    return branches


def get_git_local_head_branch() -> str:
    branch = Repo().active_branch
    return branch.name


def check_remote_branch_exist() -> bool:
    return get_git_local_head_branch() in get_git_remote_branch()


def git_push_to_remote_branch() -> str:
    if check_remote_branch_exist():
        logging.info(f" -- push to origin/{get_git_local_head_branch()}")
        git_cmd = "git push"
    else:
        git_cmd = f"git push --set-upstream origin {get_git_local_head_branch()}"
        logging.warning(f" -- Can't find branch {get_git_local_head_branch()} in repo")
        logging.warning(f" -- set-upstream origin {get_git_local_head_branch()}")
    return git_cmd


def git_push_cmd(commit_message) -> None:
    logging.info(" -- Update README.md")
    logging.info(" -- $  python3 helper.py -u")
    os.system("python3 helper.py -u")

    logging.info(" -- [Git] add files")
    logging.info(" -- $ git add .")
    os.system("git add .")

    logging.info(" -- [Git] commit")

    commit_cmd = f"git commit -m '{commit_message}'"
    logging.info(f" -- {commit_cmd}")
    os.system(commit_cmd)

    logging.info(" -- [Git] push")
    git_cmd = git_push_to_remote_branch()
    logging.info(f" -- $ {git_cmd}")
    os.system(git_cmd)

    logging.info(" -- Done --")
