import os
from resources.helpers.common import get_project_root

max_length = 140


def formatter_cmd() -> dict:
    file_list = os.listdir(get_project_root())
    flake8_folder_list = ""
    black_folder_list = ""

    cmd = {"flake8_cmd": "", "black_cmd": ""}

    for file in file_list:
        if file[0] != "." and file != "venv":
            if os.path.isdir(file):
                flake8_folder_list = flake8_folder_list + f" {file}"
                black_folder_list = black_folder_list + f" {file}/*"

            cmd["flake8_cmd"] = f"flake8 --count{flake8_folder_list} ./*.py"
            cmd["black_cmd"] = f"black --line-length {max_length}{black_folder_list} ./*.py"

    return cmd
