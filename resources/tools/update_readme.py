import logging

from resources.helpers.common import get_readme_path
from resources.utils.folder_manager import FolderManager
from resources.utils.folder_manager import write_file
from resources.utils.folder_manager import get_current_readme_file_data

logging.basicConfig(level=logging.INFO)


class UpdateReadMe:
    def __init__(self):
        self.folder = FolderManager()
        self.title = (
            "LeetCode\n========\n\n"
            f"| - | ID | Title | {'Python3'.ljust(35,' ')}"
            f"| Difficulty | Topic Tags |\n"
            f"| -- | -- | ----- | {''.ljust(43,'-')} "
            f"| ---------- | ---------- |"
        )

    def func_update_read_me(self) -> None:
        current_readme = get_current_readme_file_data()
        problems_list = self.folder.get_problem_folder_list()
        summary = self.title if len(problems_list) > 0 else ""
        count = 1
        for problem_id in problems_list:
            if int(problem_id) in current_readme:
                info = current_readme[int(problem_id)][:-1]
            else:
                info = self.folder.print_problem_info(int(problem_id))
                logging.info(f"add info :\n{info}")
            summary = summary + "\n" + "|  " + str(count) + info
            count = count + 1
        summary = summary + "\n"
        write_file(file_path=get_readme_path(), text=summary)
        logging.info(f"\n\n{summary}\n")
        logging.info("Done")
