import logging
from resources.utils.folder_manager import FolderManager

logging.basicConfig(level=logging.INFO)


class AddProblemFolder:
    def __init__(self):
        self.folder = FolderManager()

    def func_add_problem_folders(self, problems_list) -> None:
        """
        1. check folder exist
        2. if no folder : create new folder and file
        3. if folder exist : ignore
        """
        problems_list.sort()

        for problem_id in problems_list:
            assert self.folder.create_problem_folder_and_file(problem_id)
