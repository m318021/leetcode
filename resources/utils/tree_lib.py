from resources.helpers.leetcode_lib import TreeNode
import collections
import math


class Tree(TreeNode):
    def build_binary_tree(self, items: list[int]) -> TreeNode:
        """Create BT from list of values."""
        n = len(items)
        if n == 0:
            return None

        def inner(index: int = 0) -> TreeNode:
            """Closure function using recursion bo build tree"""
            if n <= index or items[index] is None:
                return None

            node = TreeNode(items[index])
            node.left = inner(2 * index + 1)
            node.right = inner(2 * index + 2)
            return node

        return inner()

    def print_binary_tree(self, root) -> None:
        queue = collections.deque()
        queue.append(root)
        count = 0

        while queue:
            node = queue.popleft()
            count = count + 1
            if not node:
                print("None ", end="")
            else:
                print("{}  ".format(node.val), end="")

                queue.append(node.left)
                queue.append(node.right)

            level = math.ceil(math.log2(count))
            # print("level = {}".format(level))
            level_count = pow(2, level) - 1
            if level_count == count or count == 1:
                print("")

        print("\n")
