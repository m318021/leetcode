from resources.helpers.leetcode_lib import ListNode
from typing import List


class LinkList(ListNode):
    def build_list(self, nums: List[int]) -> ListNode:

        if nums == []:
            return []

        list_node = []
        for i in range(len(nums)):
            list_node.append(ListNode(nums[i]))

        result = list_node[0]
        for i in range(len(nums) - 1):
            list_node[i].next = list_node[i + 1]

        return result

    def print_list(self, list_node: ListNode) -> None:

        if list_node is None or list_node == []:
            return None

        head = list_node

        while head is not None:
            print("{} -> ".format(head.val), end="")
            head = head.next
        print("Null")


if __name__ == "__main__":
    l1 = [2, 3, 4]
    link_list = LinkList()

    list_01 = link_list.build_list(l1)

    link_list.print_list(list_01)
