from resources.utils.config_manager import ConfigManager
from resources.utils.config_manager import write_yaml_file
from resources.utils.config_manager import read_yaml_file
from resources.helpers.api_request import APIRequest
from resources.helpers.common import get_project_root
from resources.helpers.common import get_total_problems
import logging
import json
import os

logging.basicConfig(level=logging.INFO)

target_ini_file = "leetcode"


def parse_id(frontend_question_id) -> str:

    p_id = int(frontend_question_id)
    if p_id < 10:
        return "000" + str(p_id)
    elif 9 < p_id < 100:
        return "00" + str(p_id)
    elif 99 < p_id < 1000:
        return "0" + str(p_id)
    else:
        return str(p_id)


def get_file_id(case_id, chunk=100) -> int:
    if int(case_id) % 100 == 0:
        file_id = int(case_id)
    else:
        file_id = (int(case_id) // chunk + 1) * chunk

    return file_id


class LeetCodeManager:
    def __init__(self):

        leetcode_config = ConfigManager(ini_file=target_ini_file).get_config()
        self.url = leetcode_config["LeetCode"]["url"]
        self.query_path = leetcode_config["LeetCode"]["query_path"]
        self.query_body = json.loads(leetcode_config["LeetCode"]["query_body"])
        self.api_request = APIRequest(server=self.url)
        self.problems_url = os.path.join(self.url, "problems")
        self.header = json.loads(leetcode_config["LeetCode"]["query_header"])
        self.chunk = int(leetcode_config["SETTING"]["file_chunk"])

    def get_problems_from_leetcode(self, limit=100, filters={}) -> json:

        self.query_body["variables"]["limit"] = limit
        self.query_body["variables"]["filters"] = filters
        data = json.dumps(self.query_body)
        headers = self.header

        response = self.api_request.post(path=self.query_path, headers=headers, data=data)
        assert response.status_code == 200
        return response.json()

    def get_leetcode_total_amount(self) -> int:
        response = self.get_problems_from_leetcode(limit=1)
        return response["data"]["problemsetQuestionList"]["total"]

    def get_current_total_amount(self) -> int:
        folder_path = get_total_problems()
        abs_folder_path = os.path.join(get_project_root(), folder_path)
        list_files = os.listdir(abs_folder_path)
        list_files.sort()
        latest_file = list_files[len(list_files) - 1]
        abs_file_path = os.path.join(abs_folder_path, latest_file)

        problems_info = read_yaml_file(abs_file_path)
        amount = list(problems_info.keys())[len(problems_info) - 1]

        return int(amount)

    def get_all_problems_from_leetcode(self) -> json:

        total_amount = self.get_leetcode_total_amount()
        response = self.get_problems_from_leetcode(limit=total_amount)
        questions = response["data"]["problemsetQuestionList"]["questions"]

        data = {}
        for question in questions:
            frontendQuestionId = question["frontendQuestionId"]

            id = parse_id(frontendQuestionId)
            temp = {
                "id": id,
                "url": os.path.join(self.problems_url, question["titleSlug"]),
                "difficulty": question["difficulty"],
                "title": question["title"],
                "titleSlug": question["titleSlug"],
                "topicTags": question["topicTags"],
            }
            data[id] = temp

        return data

    def divide_and_write_problems(self) -> None:

        problems = self.get_all_problems_from_leetcode()
        total_problems = len(problems)
        assert self.get_leetcode_total_amount() == total_problems

        files = total_problems // self.chunk + 1

        for i in range(1, files + 1):
            start = (i - 1) * self.chunk + 1
            end = i * self.chunk
            file_name = "{}.yaml".format(parse_id(end))
            logging.info("{}, {}, {}".format(start, end, file_name))
            file_path = os.path.join(get_total_problems(), file_name)

            data = {}
            for case_id in range(start, end + 1):
                if case_id < total_problems + 1:
                    id = parse_id(case_id)
                    data[id] = problems[id]
                    logging.info("ID : {}".format(case_id))

            logging.info("Generate {}".format(file_name))
            write_yaml_file(file_path=file_path, dict_file=data)
        logging.info("Done")

    def find_leetcode_file_path_by_id(self, problem_id) -> str:
        """
        1. id not exist => generate again
        2. id exist => get information
        :return file_data
        """

        file_id = get_file_id(problem_id, chunk=self.chunk)

        self.get_current_total_amount()
        if file_id > self.get_current_total_amount():
            logging.info(f"Problem : {problem_id} is over total, need to update!!")
            self.divide_and_write_problems()

        file_name = parse_id(file_id)
        file_path = os.path.join(get_total_problems(), f"{file_name}.yaml")

        return file_path
