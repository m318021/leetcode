import os.path
import logging
import json
from resources.utils.leetcode_manager import LeetCodeManager
from resources.utils.leetcode_manager import parse_id
from resources.utils.leetcode_manager import get_file_id
from resources.utils.config_manager import ConfigManager
from resources.utils.config_manager import read_yaml_file
from resources.helpers.common import get_project_root
from resources.helpers.common import get_readme_path

logging.basicConfig(level=logging.INFO)


def read_file(file_path):
    read_result = []
    with open(file_path, "r") as file:
        line = file.readline()
        while line:
            read_result.append(line)
            line = file.readline()

    return read_result


def write_file(file_path, text) -> None:
    with open(file_path, "w+") as file:
        file.write(text)


def pretty_print_json(json_obj) -> None:
    json_formatted_str = json.dumps(json_obj, indent=4)
    logging.info(json_formatted_str)


def get_current_readme_file_data() -> dict:
    readme = read_file(get_readme_path())

    result = {}

    if len(readme) > 5:
        for item in readme[5:]:
            temp = item[:-1].split("|")
            readme_id = int(temp[2])
            start = item.find("|", 2)
            result[readme_id] = item[start:]

    return result


class FolderManager:
    def __init__(self):
        config = ConfigManager(ini_file="project").get_config()
        self.leetCode = LeetCodeManager()
        self.config_info = config
        self.folder_name = config["FOLDER"]["problems"]
        self.language = config["SETTING"]["language"]
        self.default_text = "from typing import List\n" "class Solution:\n" "if __name__ == '__main__':\n" "    #input\n    #output"
        self.note_text = "# Note"

    def get_problem_info(self, problem_id) -> dict:
        file_path = self.leetCode.find_leetcode_file_path_by_id(problem_id)
        file_problem_data = read_yaml_file(file_path)
        str_problem_id = parse_id(problem_id)
        problem = file_problem_data[str_problem_id]

        return problem

    def generate_path(self, data) -> dict:

        path = {}
        root = get_project_root()

        file_id = get_file_id(case_id=data["id"], chunk=self.leetCode.chunk)

        folder_id = parse_id(file_id)
        path["folder"] = f"{self.folder_name}/{folder_id}"
        path["file_folder"] = f'{path["folder"]}/{data["id"]}-{data["titleSlug"]}'
        path["file"] = f'{path["file_folder"]}/{data["id"]}.py'
        path["note"] = f'{path["file_folder"]}/note'

        path["abs_folder"] = os.path.join(root, path["file_folder"])
        path["abs_file"] = os.path.join(root, path["file"])
        path["abs_note"] = os.path.join(root, path["note"])

        topic_tags = ""
        for topic in data["topicTags"]:
            topic_tags = topic_tags + topic["name"] + ", "

        path["topic_tags"] = topic_tags
        return path

    def print_problem_info(self, problem_id) -> str:

        problem = self.get_problem_info(problem_id)
        leet_code_id = parse_id(problem_id)

        path = self.generate_path(problem)

        add_line = "| {} | [{}]({}) | [{}](./{}) | {} | {} |".format(
            leet_code_id,
            problem["title"],
            problem["url"],
            self.language,
            path["file"],
            problem["difficulty"],
            path["topic_tags"][:-2],
        )

        return add_line

    def create_problem_folder_and_file(self, problem_id) -> bool:

        problem = self.get_problem_info(problem_id)
        path = self.generate_path(problem)

        topic_tags = "# Topic : " + path["topic_tags"]

        if os.path.isdir(path["folder"]) is False:
            os.mkdir(path["folder"])

        if os.path.isdir(path["abs_folder"]) is False:
            logging.info(f' --- Create Folder : {path["folder"]}')
            os.mkdir(path["abs_folder"])
            text = topic_tags[:-2] + "\n" + self.default_text
            logging.info(f' --- Create File : {path["file"]}')
            write_file(path["abs_file"], text)
            write_file(path["abs_note"], self.note_text)

        else:
            logging.warning(f"FIle : {path['abs_file']} exist")

        logging.info("")
        return os.path.isfile(path["abs_file"])

    def get_problem_folder_list(self) -> list:

        abs_path = os.path.join(get_project_root(), self.folder_name)
        problems_list = os.listdir(path=abs_path)
        result = []

        if problems_list is not None:
            problems_list.sort()
            for folder in problems_list:
                if folder != ".DS_Store":
                    folder_path = os.path.join(abs_path, folder)
                    files_list = os.listdir(path=folder_path)
                    files_list.sort()
                    for item in files_list:
                        if item != ".DS_Store":
                            split_folder_name = item.split("-")
                            result.append(split_folder_name[0])
        else:
            logging.warning(" ---  Problems folder is empty!!!")

        return result
