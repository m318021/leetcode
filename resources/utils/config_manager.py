import logging
import pickle
import yaml
from threading import Lock
from configparser import ConfigParser
from configparser import RawConfigParser
from resources.helpers.common import get_project_root

logger = logging.getLogger()


class Singleton(type):
    _instances = {}
    _lock: Lock = Lock()

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            with cls._lock:
                if cls not in cls._instances:
                    cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class ConfigManager:
    _path_configs = get_project_root() / "resources/config"
    __configs = {
        "project": "config_project.ini",
        "leetcode": "config_leetcode.ini",
    }

    def __init__(self, ini_file):
        config_file = self.__configs.get(ini_file)
        self._load_config(config_file, ConfigManager._path_configs)

    def _load_config(self, config: str, path) -> None:
        if ".ini" in config:
            config = config.replace(".ini", "")
        self.config = RawConfigParser()
        try:
            with open(f"{path}/{config}.ini") as file:
                self.config.read(file)
        except Exception:
            raise FileNotFoundError(f"File {config}.ini is not found!")
        self.config_path = self.config.read(f"{path}/{config}.ini")
        self.config_name = config + ".ini"
        logger.debug(f"Loaded config {path}/{config}")

    def _deep_copy(self) -> ConfigParser:
        """deep copy config"""
        temp = pickle.dumps(self.config)
        new_config = pickle.loads(temp)
        return new_config

    def get_config(self) -> ConfigParser:
        config = self._deep_copy()
        return config

    def write_and_save_config(self) -> None:
        with open(f"{self.config_path[0]}", "w") as configfile:
            self.config.write(configfile)

    def get_config_path(self) -> str:
        return self.config_path[0]

    def get_config_name(self) -> str:
        return self.config_name


def read_yaml_file(yaml_file_path=""):
    try:
        with open(yaml_file_path) as f:
            yaml_data = yaml.load(f, Loader=yaml.SafeLoader)

        return yaml_data

    except Exception:
        raise FileNotFoundError(f"File {yaml_file_path} is not found!")


def write_yaml_file(file_path, dict_file):
    try:
        with open(file_path, "w") as file:
            yaml.dump(dict_file, file)

    except Exception:
        raise FileNotFoundError(f"File {file_path} is not found!")
