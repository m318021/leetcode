# Topic : Array, Sorting
from typing import List


class Solution:
    def minimumAbsDifference(self, arr: List[int]) -> List[List[int]]:

        result, _min = {}, float("inf")
        arr.sort()

        for i in range(1, len(arr)):
            diff = abs(arr[i] - arr[i - 1])
            _min = min(_min, diff)
            if diff == _min:
                if _min not in result:
                    result[_min] = [[arr[i - 1], arr[i]]]
                else:
                    result[_min].append([arr[i - 1], arr[i]])

        return result[_min]


if __name__ == "__main__":

    arr = [4, 2, 1, 3]
    expect = [[1, 2], [2, 3], [3, 4]]
    output = Solution().minimumAbsDifference(arr)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    arr = [1, 3, 6, 10, 15]
    expect = [[1, 3]]
    output = Solution().minimumAbsDifference(arr)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    arr = [3, 8, -10, 23, 19, -4, -14, 27]
    expect = [[-14, -10], [19, 23], [23, 27]]
    output = Solution().minimumAbsDifference(arr)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
