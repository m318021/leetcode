# Topic : Array, Heap (Priority Queue)
from typing import List


class Solution:
    def lastStoneWeight(self, stones: List[int]) -> int:
        if len(stones) == 1:
            return stones[0]

        stones.sort(reverse=True)

        while len(stones) >= 2:
            y, x = stones[:2]
            if x == y:
                stones = stones[2:]
            else:
                stones = stones[2:] + [y - x]

            stones.sort(reverse=True)

        if not stones:
            return 0

        return stones[0]


if __name__ == "__main__":
    stones = [2, 7, 4, 1, 8, 1]
    output = Solution().lastStoneWeight(stones)
    print(output)

    stones = [1]
    output = Solution().lastStoneWeight(stones)
    print(output)
