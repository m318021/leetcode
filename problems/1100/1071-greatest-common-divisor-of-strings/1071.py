# Topic : Math, String
class Solution:
    def gcdOfStrings(self, str1: str, str2: str) -> str:

        _long = str1 if len(str1) >= len(str2) else str2
        _short = str1 if len(str1) < len(str2) else str2

        def divisor(lo, sh):
            if len(lo) % len(sh) == 0:
                count = len(lo) // len(sh)
                result = sh * count
                return result == lo

            return False

        result = ""
        temp = ""
        index = 0
        while index < len(_short):
            temp = temp + _short[index]
            result_l = divisor(_long, temp)
            result_s = divisor(_short, temp)

            if result_l is True and result_s is True:
                result = temp

            index += 1

        return result
