# Topic : Array, Prefix Sum
from typing import List


class Solution:
    def minStartValue(self, nums: List[int]) -> int:

        temp, _min = 0, 0

        for i in range(len(nums)):
            temp = temp + nums[i]
            if temp <= 0:
                _min = min(temp, _min)

        return 1 - _min

        # start_value = max(1 - nums[0], 1)

        # def check(start_value, nums):

        #     value = start_value
        #     for num in nums:
        #         value += num
        #         if value < 1:
        #             return False

        #     return True

        # _check = False
        # while _check is False:
        #     result = check(start_value, nums)
        #     if result is True:
        #         return start_value
        #     start_value += 1

        # return 0


if __name__ == "__main__":

    nums = [-3, 2, -3, 4, 2]
    expect = 5
    output = Solution().minStartValue(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [1, 2]
    expect = 1
    output = Solution().minStartValue(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [1, -2, -3]
    expect = 5
    output = Solution().minStartValue(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
