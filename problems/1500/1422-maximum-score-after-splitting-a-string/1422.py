# Topic : String, Prefix Sum
class Solution:
    def maxScore(self, s: str) -> int:

        l_sum = [0 for i in range(len(s))]
        r_sum = [0 for i in range(len(s))]
        l, r = 0, 0

        for i in range(len(s)):
            l_value = 1 if s[len(s) - i - 1] == "1" else 0
            r_value = 1 if s[i] == "0" else 0

            l += l_value
            r += r_value

            l_sum[len(s) - i - 1] = l
            r_sum[i] = r

        l_sum = l_sum[1:]
        r_sum = r_sum[:-1]

        _max = 0
        for i in range(len(r_sum)):
            value = l_sum[i] + r_sum[i]
            _max = max(_max, value)

        return _max


if __name__ == "__main__":

    s = "011101"
    expect = 5
    output = Solution().maxScore(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "00111"
    expect = 5
    output = Solution().maxScore(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "1111"
    expect = 3
    output = Solution().maxScore(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
