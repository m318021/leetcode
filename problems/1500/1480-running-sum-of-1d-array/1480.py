# Topic : Array, Prefix Sum
from typing import List


class Solution:
    def runningSum(self, nums: List[int]) -> List[int]:

        for i in range(1, len(nums)):
            nums[i] = nums[i] + nums[i - 1]

        return nums


if __name__ == "__main__":
    nums = [1, 2, 3, 4]
    output = Solution().runningSum(nums)
    print(output)

    nums = [1, 1, 1, 1, 1]
    output = Solution().runningSum(nums)
    print(output)

    nums = [3, 1, 2, 10, 1]
    output = Solution().runningSum(nums)
    print(output)
