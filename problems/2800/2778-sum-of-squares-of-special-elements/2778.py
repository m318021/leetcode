# Topic : Array, Enumeration
from typing import List


class Solution:
    def sumOfSquares(self, nums: List[int]) -> int:

        result = 0
        length = len(nums)

        for i in range(0, len(nums)):

            if length % (i + 1) == 0:
                result += nums[i] * nums[i]

        return result


if __name__ == "__main__":

    nums = [1, 2, 3, 4]
    expect = 21
    output = Solution().sumOfSquares(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [2, 7, 1, 19, 18, 3]
    expect = 63
    output = Solution().sumOfSquares(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
