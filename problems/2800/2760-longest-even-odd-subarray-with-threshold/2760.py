# Topic : Array, Sliding Window
from typing import List


class Solution:
    def longestAlternatingSubarray(self, nums: List[int], threshold: int) -> int:

        r, ans, n = 0, 0, len(nums)

        for i in range(n):
            if nums[i] % 2 == 0 and nums[i] <= threshold:
                r = i + 1
                while r < n and nums[r] % 2 != nums[r - 1] % 2 and nums[r] <= threshold:
                    r += 1
                ans = max(ans, r - i)

        return ans


if __name__ == "__main__":

    nums = [3, 2, 5, 4]
    threshold = 5
    expect = 3
    output = Solution().longestAlternatingSubarray(nums, threshold)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [1, 2]
    threshold = 2
    expect = 1
    output = Solution().longestAlternatingSubarray(nums, threshold)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
