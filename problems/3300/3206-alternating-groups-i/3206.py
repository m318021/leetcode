# Topic : Array, Sliding Window
from typing import List


class Solution:
    def numberOfAlternatingGroups(self, colors: List[int]) -> int:

        _colors = colors + colors[:2]
        ans = 0

        for i in range(len(colors)):

            temp = _colors[i : i + 3]
            if _colors[i] == 0 and temp == [0, 1, 0]:
                ans += 1

            if _colors[i] == 1 and temp == [1, 0, 1]:
                ans += 1

        return ans


if __name__ == "__main__":

    colors = [1, 1, 1]
    expect = 0
    output = Solution().numberOfAlternatingGroups(colors)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    colors = [0, 1, 0, 0, 1]
    expect = 3
    output = Solution().numberOfAlternatingGroups(colors)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    colors = [0, 1, 0]
    expect = 1
    output = Solution().numberOfAlternatingGroups(colors)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
