# Topic : Hash Table, String, Bit Manipulation, Counting
class Solution:
    def repeatedCharacter(self, s: str) -> str:

        map = []

        for ch in s:
            if ch in map:
                return ch
            map.append(ch)

        return ""


if __name__ == "__main__":

    s = "abccbaacz"
    expect = "c"
    output = Solution().repeatedCharacter(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "abcdd"
    expect = "d"
    output = Solution().repeatedCharacter(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
