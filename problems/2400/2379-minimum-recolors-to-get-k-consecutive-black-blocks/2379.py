# Topic : String, Sliding Window


class Solution:
    def minimumRecolors(self, blocks: str, k: int) -> int:

        start, length, count, result = 0, 0, 0, len(blocks)

        for i in range(len(blocks)):
            if blocks[i] == "W":
                count += 1
            length += 1

            if length == k:
                result = min(result, count)
                length -= 1
                if blocks[start] == "W":
                    count -= 1
                start += 1

        return result


if __name__ == "__main__":

    blocks = "WBBWWBBWBW"
    k = 7
    expect = 3
    output = Solution().minimumRecolors(blocks, k)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    blocks = "WBWBBBW"
    k = 2
    expect = 0
    output = Solution().minimumRecolors(blocks, k)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
