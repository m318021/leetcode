# Topic : Array, Hash Table, Greedy, Sorting, Heap (Priority Queue), Simulation
from typing import List


class Solution:
    def minimumOperations(self, nums: List[int]) -> int:

        # method 01
        # set_num = set(nums)
        # result = set_num -set([0])

        # return len(result)

        # method 02
        repeat = {}
        for num in nums:
            if num not in repeat:
                if num != 0:
                    repeat[num] = 1

        return len(repeat)


if __name__ == "__main__":

    nums = [1, 5, 0, 3, 5]
    expect = 3
    output = Solution().minimumOperations(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [0]
    expect = 0
    output = Solution().minimumOperations(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
