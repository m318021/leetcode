# Topic : Hash Table, String, Sorting, Heap (Priority Queue), Bucket Sort, Counting
class Solution:
    def frequencySort(self, s: str) -> str:

        map = {}
        set_s = set(s)
        for ch in set_s:
            map[ch] = 0

        head, tail = 0, len(s) - 1
        while head <= tail:
            if s[head] in map:
                map[s[head]] += 1
            if s[tail] in map:
                map[s[tail]] += 1

            head += 1
            tail -= 1

        if len(s) % 2 == 1:
            map[s[tail + 1]] -= 1

        sorted_list = sorted(map.items(), key=lambda x: x[1])

        result = ""

        for item in reversed(sorted_list):
            result = result + item[0] * item[1]

        return result


if __name__ == "__main__":

    s = "tree"
    expect = "eert"
    output = Solution().frequencySort(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "cccaaa"
    expect = "cccaaa"
    output = Solution().frequencySort(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "Aabb"
    expect = "bbaA"
    output = Solution().frequencySort(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "Abba"
    expect = "bbaA"
    output = Solution().frequencySort(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
