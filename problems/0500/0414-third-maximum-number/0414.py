# Topic : Array, Sorting
from typing import List


class Solution:
    def thirdMax(self, nums: List[int]) -> int:

        _nums = list(set(nums))
        _nums.sort()

        if len(_nums) < 3:
            return _nums[-1]

        return _nums[-3]
