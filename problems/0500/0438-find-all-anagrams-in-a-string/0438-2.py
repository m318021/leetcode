# Topic : Hash Table, String, Sliding Window
from typing import List


class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:

        result = []
        p_map = {}
        for char in p:
            if char not in p_map:
                p_map[char] = 1
            else:
                p_map[char] += 1

        for i in range(len(s) - len(p) + 1):
            valid = True
            data = {}

            for j in range(i, i + len(p)):
                if s[j] in p_map:
                    if s[j] not in data:
                        data[s[j]] = 1
                    else:
                        data[s[j]] += 1
                else:
                    valid = False

            if valid and data == p_map:
                result.append(i)

        return result


if __name__ == "__main__":
    s = "cbaebabacd"
    p = "abc"
    output = Solution().findAnagrams(s, p)
    print(output)

    s = "abab"
    p = "ab"
    output = Solution().findAnagrams(s, p)
    print(output)

    s = "baa"
    p = "aa"
    output = Solution().findAnagrams(s, p)
    print(output)
