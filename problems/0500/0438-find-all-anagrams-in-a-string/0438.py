# Topic : Hash Table, String, Sliding Window
from typing import List


class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:

        result = []
        start, end = 0, 0
        match = 0
        table_p = {}
        for i in range(len(p)):
            table_p[p[i]] = table_p.get(p[i], 0) + 1

        for end in range(len(s)):
            if s[end] in table_p:
                table_p[s[end]] -= 1

                if table_p[s[end]] == 0:
                    match += 1

            if match == len(table_p):
                result.append(start)

            if end >= len(p) - 1:
                if s[start] in table_p:
                    if table_p[s[start]] == 0:
                        match = match - 1
                    table_p[s[start]] += 1
                start += 1

        return result


if __name__ == "__main__":
    s = "cbaebabacd"
    p = "abc"
    output = Solution().findAnagrams(s, p)
    print(output)

    s = "abab"
    p = "ab"
    output = Solution().findAnagrams(s, p)
    print(output)

    s = "baa"
    p = "aa"
    output = Solution().findAnagrams(s, p)
    print(output)
