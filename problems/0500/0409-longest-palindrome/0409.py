# Topic : Hash Table, String, Greedy


class Solution:
    def longestPalindrome(self, s: str) -> int:
        if s == "":
            return 0

        table = {}
        for char in s:
            if char not in table:
                table[char] = 1
            else:
                table[char] += 1

        odd_value = 0
        for key in table:
            if table[key] % 2 == 1:
                odd_value += 1

        total = len(s) - odd_value + 1 if odd_value > 0 else len(s)

        return total


if __name__ == "__main__":
    s = "abccccdd"
    output = Solution().longestPalindrome(s)
    print(output)

    s = "a"
    output = Solution().longestPalindrome(s)
    print(output)
