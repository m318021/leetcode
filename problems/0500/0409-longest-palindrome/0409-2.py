# Topic : Hash Table, String, Greedy


class Solution:
    def longestPalindrome(self, s: str) -> int:
        # Create a hash map to count the number of occurrences of each letter
        letter_counts = {}
        for c in s:
            if c in letter_counts:
                letter_counts[c] += 1
            else:
                letter_counts[c] = 1

        # Initialize the length of the palindrome and a flag to indicate whether it has an odd length
        palindrome_length = 0
        has_odd_length = False

        # Iterate through the hash map and add the number of occurrences of each letter to the palindrome length
        for letter, count in letter_counts.items():
            if count % 2 == 0:
                # If the letter appears an even number of times, add it to the palindrome length
                palindrome_length += count
            else:
                # If the letter appears an odd number of times, add it to the palindrome length minus one
                # and set the flag to indicate that the palindrome has an odd length
                palindrome_length += count - 1
                has_odd_length = True

        # If the palindrome has an odd length, add one to the palindrome length to account for the letter
        # that appears an odd number of times
        if has_odd_length:
            palindrome_length += 1

        return palindrome_length


if __name__ == "__main__":
    s = "abccccdd"
    output = Solution().longestPalindrome(s)
    print(output)

    s = "a"
    output = Solution().longestPalindrome(s)
    print(output)
