# Topic : Array, Two Pointers, Greedy, Sorting
from typing import List


class Solution:
    def findContentChildren(self, g: List[int], s: List[int]) -> int:

        g.sort()
        s.sort()

        current_g = 0

        for i in range(len(s)):

            if current_g < len(g):
                if s[i] >= g[current_g]:
                    current_g += 1
            else:
                return current_g

        return current_g
