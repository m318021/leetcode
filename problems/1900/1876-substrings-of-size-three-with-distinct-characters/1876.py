# Topic : Hash Table, String, Sliding Window, Counting
class Solution:
    def countGoodSubstrings(self, s: str) -> int:
        if len(s) < 3:
            return 0

        start = 0
        end = 3
        count = 0

        while end <= len(s):
            print(set(s[start:end]))
            if len(set(s[start:end])) == 3:
                count += 1
            start += 1
            end += 1

        return count


if __name__ == "__main__":
    s = "xyzzaz"
    output = Solution().countGoodSubstrings(s)
    print(output)

    s = "aababcabc"
    output = Solution().countGoodSubstrings(s)
    print(output)

    s = "owuxoelszb"
    output = Solution().countGoodSubstrings(s)
    print(output)
