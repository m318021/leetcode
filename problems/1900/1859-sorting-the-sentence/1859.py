# Topic : String, Sorting
class Solution:
    def sortSentence(self, s: str) -> str:

        _s = s.split(" ")
        map = ["" for i in range(len(_s))]

        for sub in _s:
            index = 0
            _str = ""
            for ch in sub:
                if ch in "0123456789":
                    index = index * 10 + int(ch)
                else:
                    _str = _str + ch
            map[index - 1] = _str

        result = ""
        for sub_str in map:
            result = result + sub_str + " "

        return result[:-1]


if __name__ == "__main__":

    s = "is2 sentence4 This1 a3"
    expect = "This is a sentence"
    output = Solution().sortSentence(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "Myself2 Me1 I4 and3"
    expect = "Me Myself and I"
    output = Solution().sortSentence(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
