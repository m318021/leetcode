# Topic : Array, Hash Table, String
from typing import List


class Solution:
    def oddString(self, words: List[str]) -> str:
        maps = {}
        for i in range(len(words)):
            temp = []
            for j in range(1, len(words[i])):
                temp.append(ord(words[i][j]) - ord(words[i][j - 1]))
            if str(temp) not in maps:
                maps[str(temp)] = [words[i]]
            else:
                maps[str(temp)].append(words[i])

        for key in maps:
            if len(maps[key]) == 1:
                return maps[key][0]


if __name__ == "__main__":
    words = ["adc", "wzy", "abc"]
    expect = "abc"
    output = Solution().oddString(words)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    words = ["aaa", "bob", "ccc", "ddd"]
    expect = "bob"
    output = Solution().oddString(words)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
