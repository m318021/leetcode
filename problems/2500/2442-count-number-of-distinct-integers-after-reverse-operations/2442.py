# Topic : Array, Hash Table, Math
from typing import List


class Solution:
    def countDistinctIntegers(self, nums: List[int]) -> int:
        def reverse(num):
            str_num = str(num)

            return int(str_num[::-1])

        map = {}

        for i, num in enumerate(nums):
            if num not in map:
                map[num] = 0
            _num = reverse(num)
            if _num not in map:
                map[_num] = 0

        return len(map)


if __name__ == "__main__":
    nums = [1, 13, 10, 12, 31]
    print(Solution().countDistinctIntegers(nums))

    nums = [2, 2, 2]
    print(Solution().countDistinctIntegers(nums))
