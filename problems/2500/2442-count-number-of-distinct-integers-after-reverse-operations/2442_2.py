# Topic : Array, Hash Table, Math
from typing import List


class Solution:
    def countDistinctIntegers(self, nums: List[int]) -> int:
        def reverse(num):
            str_num = str(num)

            return int(str_num[::-1])

        nums = set(nums)
        map = list(nums)

        for num in nums:
            _num = reverse(num)
            map.append(_num)

        map = set(map)

        return len(map)


if __name__ == "__main__":
    nums = [1, 13, 10, 12, 31]
    print(Solution().countDistinctIntegers(nums))

    nums = [2, 2, 2]
    print(Solution().countDistinctIntegers(nums))
