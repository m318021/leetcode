# Topic : Math
from typing import List


class Solution:
    def convertTemperature(self, celsius: float) -> List[float]:

        kevin = celsius + 273.15
        fahrenheit = celsius * 1.8 + 32

        return [kevin, fahrenheit]
