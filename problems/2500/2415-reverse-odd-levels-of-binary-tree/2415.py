# Topic : Tree, Depth-First Search, Breadth-First Search, Binary Tree
import collections
from typing import Optional
from resources.helpers.leetcode_lib import TreeNode


class Solution:
    def reverseOddLevels(self, root: Optional[TreeNode]) -> Optional[TreeNode]:

        if not root:
            return None

        tree_queue = collections.deque()
        tree_queue.append([root])

        depth = 0

        while tree_queue:
            temp_queue = []
            node_list = tree_queue.pop()

            if depth % 2 == 1:
                l, r = 0, len(node_list) - 1
                while l < r:
                    temp = node_list[r].val
                    node_list[r].val = node_list[l].val
                    node_list[l].val = temp

                    l += 1
                    r -= 1

            for node in node_list:
                if node.left is not None:
                    temp_queue.append(node.left)
                if node.right is not None:
                    temp_queue.append(node.right)

            depth += 1

            if temp_queue != []:
                tree_queue.append(temp_queue)

        return root
