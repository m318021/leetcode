# Topic : Two Pointers, String, Dynamic Programming


class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:

        if len(s) == 0:
            return True
        if len(t) == 0:
            return False

        left_t, right_t = 0, len(t) - 1
        left_s, right_s = 0, len(s) - 1

        while left_t <= right_t:

            if left_t != right_t:
                if t[left_t] == s[left_s]:
                    left_s += 1
                if t[right_t] == s[right_s]:
                    right_s -= 1
            if left_t == right_t:
                if t[left_t] == s[left_s]:
                    left_s += 1
                elif t[right_t] == s[right_s]:
                    right_s -= 1

            left_t = left_t + 1
            right_t = right_t - 1

        return left_s > right_s


if __name__ == "__main__":
    s = "abc"
    t = "ahbgdc"
    output = Solution().isSubsequence(s, t)
    print(output)

    s = "axc"
    t = "ahbgdc"
    output = Solution().isSubsequence(s, t)
    print(output)
