# Topic : Hash Table, String, Queue, Counting
class Solution:
    def firstUniqChar(self, s: str) -> int:

        map = {}

        for i, ch in enumerate(s):
            if ch not in map:
                map[ch] = [i]
            else:
                map[ch].append(i)

        for key in map:
            if len(map[key]) == 1:
                return map[key][0]

        return -1


if __name__ == "__main__":

    s = "leetcode"
    expect = 0
    output = Solution().firstUniqChar(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "loveleetcode"
    expect = 2
    output = Solution().firstUniqChar(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "aabb"
    expect = -1
    output = Solution().firstUniqChar(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
