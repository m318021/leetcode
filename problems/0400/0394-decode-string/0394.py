# Topic : String, Stack, Recursion
class Solution:
    def decodeString(self, s: str) -> str:

        stack = []

        for i in range(len(s)):
            if s[i] != "]":
                stack.append(s[i])
            else:
                sub_str = ""
                while stack[-1] != "[":
                    sub_str = stack.pop() + sub_str

                stack.pop()

                k = ""
                while stack and stack[-1].isdigit():
                    k = stack.pop() + k

                stack.append(int(k) * sub_str)

        return "".join(stack)


if __name__ == "__main__":
    s = "3[a]2[bc]"
    Output = "aaabcbc"
    result = Solution().decodeString(s)
    print(result)
    assert result == Output
    print("=====")
    s = "3[a2[c]]"
    Output = "accaccacc"
    result = Solution().decodeString(s)
    print(result)
    assert result == Output
    print("=====")
    s = "2[abc]3[cd]ef"
    Output = "abcabccdcdcdef"
    result = Solution().decodeString(s)
    print(result)
    assert result == Output
    print("=====")
