# Topic : Linked List
from typing import Optional
from resources.utils.link_list_lib import LinkList
from resources.utils.link_list_lib import ListNode


class Solution:
    def oddEvenList(self, head: Optional[ListNode]) -> Optional[ListNode]:

        if head is None:
            return None

        odd = head
        even = head.next
        even_head = even

        while even and even.next:
            odd.next = even.next
            odd = odd.next
            even.next = odd.next
            even = even.next

        odd.next = even_head

        return head


if __name__ == "__main__":
    input = [1, 2, 3, 4, 5]
    head = LinkList().build_list(input)
    print("input")
    LinkList().print_list(head)
    output = Solution().oddEvenList(head)
    print("output:")
    LinkList().print_list(output)

    input = [2, 1, 3, 5, 6, 4, 7]
    head = LinkList().build_list(input)
    print("input")
    LinkList().print_list(head)
    output = Solution().oddEvenList(head)
    print("output:")
    LinkList().print_list(output)
