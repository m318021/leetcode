# Topic : Math, Binary Search
class Solution:
    def isPerfectSquare(self, num: int) -> bool:

        head, tail = 0, num

        while head < tail:
            mid = (head + tail + 1) // 2
            if mid > num // mid:
                tail = mid - 1
            else:
                head = mid

        return head * head == num


if __name__ == "__main__":

    num = 16
    expect = True
    output = Solution().isPerfectSquare(num)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    num = 14
    expect = False
    output = Solution().isPerfectSquare(num)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
