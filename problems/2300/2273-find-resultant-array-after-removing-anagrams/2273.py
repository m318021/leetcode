# Topic : Array, Hash Table, String, Sorting
from typing import List


class Solution:
    def removeAnagrams(self, words: List[str]) -> List[str]:

        result = [words[0]]
        for i, w in enumerate(words):
            if i > 0:
                if sorted(w) != sorted(words[i - 1]):
                    result.append(w)

        return result

        """ method 2"""
        # return [
        #     w
        #     for i, w in enumerate(words)
        #     if i == 0 or sorted(w) != sorted(words[i - 1])
        # ]


if __name__ == "__main__":

    words = ["abba", "baba", "bbaa", "cd", "cd"]
    expect = ["abba", "cd"]
    output = Solution().removeAnagrams(words)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    words = ["a", "b", "c", "d", "e"]
    expect = ["a", "b", "c", "d", "e"]
    output = Solution().removeAnagrams(words)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
