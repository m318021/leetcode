# Topic : Math, String, Sliding Window


class Solution:
    def divisorSubstrings(self, num: int, k: int) -> int:

        str_num = str(num)

        start = 0
        result = 0
        for i in range(len(str_num)):
            length = i - start + 1
            if length == k:
                end = i + 1
                sub_string = int(str_num[start:end])
                if sub_string != 0 and num % sub_string == 0:
                    result += 1

                start += 1

        return result

        """ Method 2 """
        # _num, ans = str(num), 0
        # if k > len(_num):
        #     return 0

        # for i in range(k - 1, len(_num)):
        #     _s = int(_num[i - k + 1 : i + 1])
        #     if _s != 0 and num % _s == 0:
        #         ans += 1

        # return ans


if __name__ == "__main__":
    num = 240
    k = 2

    output = Solution().divisorSubstrings(num, k)
    print(output)

    num = 430043
    k = 2
    output = Solution().divisorSubstrings(num, k)
    print(output)

    num = 2
    k = 1
    output = Solution().divisorSubstrings(num, k)
    print(output)

    num = 10
    k = 1
    output = Solution().divisorSubstrings(num, k)
    print(output)
