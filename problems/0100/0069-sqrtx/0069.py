# Topic : Math, Binary Search
class Solution:
    def mySqrt(self, x: int) -> int:

        head, tail = 0, x

        while tail > head:
            mid = (tail + head + 1) // 2
            if mid > x // mid:
                tail = mid - 1
            else:
                head = mid

        return head


if __name__ == "__main__":

    x = 4
    expect = 2
    output = Solution().mySqrt(x)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    x = 8
    expect = 2
    output = Solution().mySqrt(x)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
