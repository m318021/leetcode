# Topic : Linked List, Math, Recursion
from resources.utils.link_list_lib import LinkList
from resources.helpers.leetcode_lib import ListNode
from typing import Optional


class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:

        ans = head = ListNode(0)
        carry = 0
        cur = None
        while l1 and l2:
            temp = l1.val + l2.val + carry
            head.next = ListNode(temp % 10)
            carry = temp // 10
            head = head.next
            l1, l2 = l1.next, l2.next

            cur = l1 if l1 else l2

        while cur:
            temp = cur.val + carry
            head.next = ListNode(temp % 10)
            carry = temp // 10

            head = head.next
            cur = cur.next

        if carry == 1:
            head.next = ListNode(1)

        return ans.next


if __name__ == "__main__":
    l1 = [2, 4, 3]
    l2 = [5, 6, 4]
    list_01 = LinkList().build_list(l1)
    list_02 = LinkList().build_list(l2)
    output = Solution().addTwoNumbers(list_01, list_02)
    LinkList().print_list(output)

    l1 = [0]
    l2 = [0]
    list_01 = LinkList().build_list(l1)
    list_02 = LinkList().build_list(l2)
    output = Solution().addTwoNumbers(list_01, list_02)
    LinkList().print_list(output)

    l1 = [9, 9, 9, 9, 9, 9, 9]
    l2 = [9, 9, 9, 9]
    list_01 = LinkList().build_list(l1)
    list_02 = LinkList().build_list(l2)
    output = Solution().addTwoNumbers(list_01, list_02)
    LinkList().print_list(output)

    l1 = []
    l2 = []
    list_01 = LinkList().build_list(l1)
    list_02 = LinkList().build_list(l2)
    output = Solution().addTwoNumbers(list_01, list_02)
    LinkList().print_list(output)
