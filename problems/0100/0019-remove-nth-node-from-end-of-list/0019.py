# Topic : Linked List, Two Pointers
from typing import Optional
from resources.utils.link_list_lib import ListNode
from resources.utils.link_list_lib import LinkList

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:
        if not head:
            return None

        slow = head
        fast = head

        for _ in range(n):
            fast = fast.next

        if fast is None:
            return slow.next

        while fast is not None and fast.next is not None:
            slow = slow.next
            fast = fast.next

        slow.next = slow.next.next

        return head


if __name__ == "__main__":
    head = [1, 2, 3, 4, 5]
    n = 2
    list_01 = LinkList().build_list(head)
    print("Input:")
    LinkList().print_list(list_01)
    output = Solution().removeNthFromEnd(list_01, n)
    print("Output:")
    LinkList().print_list(output)

    head = [1]
    n = 1
    list_01 = LinkList().build_list(head)
    print("Input:")
    LinkList().print_list(list_01)
    output = Solution().removeNthFromEnd(list_01, n)
    print("Output:")
    LinkList().print_list(output)

    head = [1, 2]
    n = 1
    list_01 = LinkList().build_list(head)
    print("Input:")
    LinkList().print_list(list_01)
    output = Solution().removeNthFromEnd(list_01, n)
    print("Output:")
    LinkList().print_list(output)
