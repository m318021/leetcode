# Topic : Math, String, Simulation


class Solution:
    def multiply(self, num1: str, num2: str) -> str:

        result = 0
        list_num1 = [int(num1[i]) for i in range(len(num1))]
        list_num2 = [int(num2[i]) for i in range(len(num2))]

        list_num1 = list_num1[::-1]
        list_num2 = list_num2[::-1]

        for i in range(len(list_num1)):
            sub_result = 0

            for j in range(len(list_num2)):
                temp = list_num1[i] * list_num2[j] * 10 ** (i + j)
                sub_result += temp

            result += sub_result

        return str(result)


if __name__ == "__main__":
    num1 = "2"
    num2 = "3"
    output = Solution().multiply(num1, num2)
    print(output)

    num1 = "123"
    num2 = "456"
    output = Solution().multiply(num1, num2)
    print(output)

    num1 = "9"
    num2 = "99"
    output = Solution().multiply(num1, num2)
    print(output)
