# Topic : Math, String, Bit Manipulation, Simulation
class Solution:
    def addBinary(self, a: str, b: str) -> str:

        a = a[::-1]
        b = a[::-1]

        length = len(a) if len(a) > len(b) else len(b)

        count = 0
        carry = 0
        result = ""
        while length > count:
            value_a = int(a[count]) if count < len(a) else 0
            value_b = int(b[count]) if count < len(b) else 0

            value = value_a + value_b + carry
            if value == 0:
                carry = 0
                result = result + "0"
            elif value == 1:
                carry = 0
                result = result + "1"
            elif value == 2:
                carry = 1
                result = result + "0"
            elif value == 3:
                carry = 1
                result = result + "1"
            count = count + 1

        if carry == 1:
            result = result + str(carry)

        return result[::-1]

        # def convert_to_decimal(_a:str):
        #     pow = 1
        #     decimal = 0
        #     for i in range(len(_a)):
        #         decimal += int(_a[len(_a)-1-i])*pow
        #         pow = pow * 2

        #     return decimal

        # decimal_a = convert_to_decimal(a)
        # decimal_b = convert_to_decimal(b)

        # sum = decimal_a + decimal_b

        # numbers = ''
        # while True:
        #     bit = sum // 2
        #     b = sum % 2
        #     numbers += str(b)
        #     sum = bit
        #     if sum == 0:
        #         break

        # return (numbers[::-1])


if __name__ == "__main__":
    # input
    # output
    a = "11"
    b = "1"
    print(Solution().addBinary(a, b))

    a = "1010"
    b = "1011"
    print(Solution().addBinary(a, b))
