# Topic : Hash Table, Math, String
class Solution:
    def intToRoman(self, num: int) -> str:

        map = [
            (1000, "M"),
            (900, "CM"),
            (500, "D"),
            (400, "CD"),
            (100, "C"),
            (90, "XC"),
            (50, "L"),
            (40, "XL"),
            (10, "X"),
            (9, "IX"),
            (5, "V"),
            (4, "IV"),
            (1, "I"),
        ]
        result = ""
        count = 0

        while num > 0:
            if num >= map[count][0]:
                temp = num // map[count][0]
                num = num - temp * map[count][0]
                for i in range(temp):
                    result = result + map[count][1]

            count += 1

        return result


if __name__ == "__main__":

    num = 3749
    output = "MMMDCCXLIX"

    result = Solution().intToRoman(num)
    print(f"num = {num}")
    print(f"result = {result}, {result == output}\n")

    num = 58
    output = "LVIII"

    result = Solution().intToRoman(num)
    print(f"num = {num}")
    print(f"result = {result}, {result == output}\n")

    num = 1994
    output = "MCMXCIV"

    result = Solution().intToRoman(num)
    print(f"num = {num}")
    print(f"result = {result}, {result == output}\n")
