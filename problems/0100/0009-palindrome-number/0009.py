# Topic : Math
class Solution:
    def isPalindrome(self, x: int) -> bool:
        if x < 0:
            return False

        reverse = 0
        check = x

        while check > 0:
            reverse = reverse * 10 + check % 10
            check = check // 10

        return reverse == x


if __name__ == "__main__":
    x = 121
    output = Solution().isPalindrome(x)
    print(output)

    x = -121
    output = Solution().isPalindrome(x)
    print(output)

    x = 10
    output = Solution().isPalindrome(x)
    print(output)
