# Topic : Tree, Depth-First Search, Binary Search Tree, Binary Tree
from typing import Optional
from resources.utils.tree_lib import Tree
from resources.utils.tree_lib import TreeNode

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right


class Solution:
    def isValidBST(self, root: Optional[TreeNode]) -> bool:
        return self.check_tree(root, float("-inf"), float("inf"))

    def check_tree(self, root, min, max):
        if root is None:
            return True
        if root.val >= max or root.val <= min:
            return False
        return self.check_tree(root.left, min, root.val) and self.check_tree(root.right, root.val, max)


if __name__ == "__main__":
    root = [2, 1, 3]
    tree_01 = Tree().build_binary_tree(root)
    Tree().print_binary_tree(tree_01)
    output = Solution().isValidBST(tree_01)
    print(output)
    print("\n")

    root = [5, 1, 4, None, None, 3, 6]
    tree_01 = Tree().build_binary_tree(root)
    Tree().print_binary_tree(tree_01)
    output = Solution().isValidBST(tree_01)
    print(output)
    print("\n")
