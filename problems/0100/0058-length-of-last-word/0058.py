# Topic : String
class Solution:
    def lengthOfLastWord(self, s: str) -> int:

        length = 0
        count = 0

        while count < len(s):
            if s[len(s) - 1 - count] == " ":
                if length > 0:
                    return length
            else:
                length += 1
            count += 1

        return length


if __name__ == "__main__":

    s = "Hello World"
    expect = 5
    ans = Solution().lengthOfLastWord(s=s)
    result = "Passed" if ans == expect else "Failed"
    print(f"s = {s}")
    print(f"result = {ans}, {result}\n")

    s = "   fly me   to   the moon  "
    expect = 4
    ans = Solution().lengthOfLastWord(s=s)
    result = "Passed" if ans == expect else "Failed"
    print(f"s = {s}")
    print(f"result = {ans}, {result}\n")

    s = "luffy is still joyboy"
    expect = 6
    ans = Solution().lengthOfLastWord(s=s)
    result = "Passed" if ans == expect else "Failed"
    print(f"s = {s}")
    print(f"result = {ans}, {result}\n")

    s = "a"
    expect = 1
    ans = Solution().lengthOfLastWord(s=s)
    result = "Passed" if ans == expect else "Failed"
    print(f"s = {s}")
    print(f"result = {ans}, {result}\n")
