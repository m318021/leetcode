# Topic : Array, Two Pointers
from typing import List


class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:

        length = 0
        for i in range(len(nums)):
            if nums[i] != val:
                nums[length] = nums[i]
                length += 1

        return length


if __name__ == "__main__":
    # input
    nums = [3, 2, 2, 3]
    val = 3
    print(Solution().removeElement(nums, val))

    nums = [0, 1, 2, 2, 3, 0, 4, 2]
    val = 2
    print(Solution().removeElement(nums, val))

    # output
