# Topic : String
class Solution:
    def myAtoi(self, s: str) -> int:

        s = s.strip()
        if not s:
            return 0

        if s[0] not in "+-1234567890":
            return 0

        result = 0
        postive = 1

        start = 0
        if s[0] in ["+", "-"]:
            postive = -1 if s[0] == "-" else 1
            start = 1

        for i in range(start, len(s)):
            if s[i] in "0123456789":
                result = result * 10 + int(s[i])
            else:
                break

        result = result * postive

        if result < 0:
            num = max(-(2**31), result)
        else:
            num = min(2**31 - 1, result)

        return num


if __name__ == "__main__":

    s = "42"
    output = 42
    ans = Solution().myAtoi(s=s)
    result = "PASS\n" if ans == output else "FAILED\n"
    print(f"output = {output}, ans = {ans}, result = {result}")

    s = " -042"
    output = -42
    ans = Solution().myAtoi(s=s)
    result = "PASS\n" if ans == output else "FAILED\n"
    print(f"output = {output}, ans = {ans}, result = {result}")

    s = "1337c0d3"
    output = 1337
    ans = Solution().myAtoi(s=s)
    result = "PASS\n" if ans == output else "FAILED\n"
    print(f"output = {output}, ans = {ans}, result = {result}")

    s = "0-1"
    output = 0
    ans = Solution().myAtoi(s=s)
    result = "PASS\n" if ans == output else "FAILED\n"
    print(f"output = {output}, ans = {ans}, result = {result}")

    s = "+-12"
    output = 0
    ans = Solution().myAtoi(s=s)
    result = "PASS\n" if ans == output else "FAILED\n"
    print(f"output = {output}, ans = {ans}, result = {result}")

    s = "+1"
    output = 1
    ans = Solution().myAtoi(s=s)
    result = "PASS\n" if ans == output else "FAILED\n"
    print(f"output = {output}, ans = {ans}, result = {result}")

    s = "-91283472332"
    output = -2147483648
    ans = Solution().myAtoi(s=s)
    result = "PASS\n" if ans == output else "FAILED\n"
    print(f"output = {output}, ans = {ans}, result = {result}")

    s = "words and 987"
    output = 0
    ans = Solution().myAtoi(s=s)
    result = "PASS\n" if ans == output else "FAILED\n"
    print(f"output = {output}, ans = {ans}, result = {result}")
