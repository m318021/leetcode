# Topic : Array, Hash Table
from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:

        map = {}

        for i in range(len(nums)):
            if nums[i] not in map:
                map[target - nums[i]] = i
            else:
                return [map[nums[i]], i]

        return []


if __name__ == "__main__":
    nums = [2, 7, 11, 15]
    target = 9

    output = Solution().twoSum(nums, target)
    print(output)

    nums = [3, 2, 4]
    target = 6

    output = Solution().twoSum(nums, target)
    print(output)

    nums = [3, 3]
    target = 6

    output = Solution().twoSum(nums, target)
    print(output)
