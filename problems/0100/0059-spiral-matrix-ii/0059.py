# Topic : Array, Matrix, Simulation
from typing import List


class Solution:
    def generateMatrix(self, n: int) -> List[List[int]]:

        result = []
        for i in range(n):
            temp = []
            for j in range(n):
                temp.append(0)
            result.append(temp)

        row_start, row_end, col_start, col_end = 0, n - 1, 0, n - 1
        count, index = 0, 0

        while index < n * n:

            for i in range(row_start, row_end + 1):
                count += 1
                result[col_start][i] = count

            for j in range(col_start + 1, col_end + 1):
                count += 1
                result[j][row_end] = count

            if row_start < row_end and col_start < col_end:
                for i in range(row_end - 1, row_start, -1):
                    count += 1
                    result[col_end][i] = count

                for j in range(col_end, col_start, -1):
                    count += 1
                    result[j][row_start] = count

            index += 1
            row_start += 1
            col_start += 1
            row_end -= 1
            col_end -= 1

        return result

        return result


if __name__ == "__main__":

    n = 3
    expect = [[1, 2, 3], [8, 9, 4], [7, 6, 5]]
    output = Solution().generateMatrix(n)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    n = 1
    expect = [[1]]
    output = Solution().generateMatrix(n)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
