# Topic : Array, Math
from typing import List


class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:

        # method 1

        for i in reversed(range(len(digits))):
            if digits[i] == 9:
                digits[i] == 0
            else:
                digits[i] = digits[i] + 1
                return digits

            return [1] + digits

        # method 2
        # carry = 1

        # for i in reversed(range(len(digits))):
        #     value = carry + digits[i]
        #     carry = value // 10
        #     value = value % 10
        #     digits[i] = value

        # if carry == 1:
        #     digits = [1] + digits

        # return digits


if __name__ == "__main__":
    # input
    # output
    digits = [1, 2, 3]
    print(Solution().plusOne(digits=digits))

    digits = [4, 3, 2, 1]
    print(Solution().plusOne(digits=digits))

    digits = [9]
    print(Solution().plusOne(digits=digits))
