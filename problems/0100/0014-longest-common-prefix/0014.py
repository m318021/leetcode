# Topic : String
from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        if not strs:
            return ""

        result = ""
        for i in range(len(strs[0])):
            for j in range(1, len(strs)):
                if i >= len(strs[j]) or strs[j][i] != strs[0][i]:
                    return result
            result = result + strs[0][i]

        return result


if __name__ == "__main__":
    strs = ["flower", "flow", "flight"]
    output = Solution().longestCommonPrefix(strs)
    print(output)

    strs = ["dog", "racecar", "car"]
    output = Solution().longestCommonPrefix(strs)
    print(output)
