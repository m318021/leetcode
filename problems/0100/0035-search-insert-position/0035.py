# Topic : Array, Binary Search
from typing import List


class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:

        for i, num in enumerate(nums):
            if target <= num:
                return i

        return len(nums)


if __name__ == "__main__":
    # input
    # output
    nums = [1, 3, 5, 6]
    target = 5
    print(Solution().searchInsert(nums=nums, target=target))

    nums = [1, 3, 5, 6]
    target = 2
    print(Solution().searchInsert(nums=nums, target=target))

    nums = [1, 3, 5, 6]
    target = 7
    print(Solution().searchInsert(nums=nums, target=target))
