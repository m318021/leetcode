# Topic : Array, Two Pointers, Greedy
from typing import List


class Solution:
    def maxArea(self, height: List[int]) -> int:

        l, r, d, area = 0, len(height) - 1, 0, 0

        while l < r:
            if height[l] <= height[r]:
                h = height[l]
                d = r - l
                l += 1
            else:
                h = height[r]
                d = r - l
                r -= 1
            _area = h * d
            area = max(area, _area)

        return area


if __name__ == "__main__":

    height = [1, 8, 6, 2, 5, 4, 8, 3, 7]
    expect = 49
    output = Solution().maxArea(height)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    height = [1, 1]
    expect = 1
    output = Solution().maxArea(height)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
