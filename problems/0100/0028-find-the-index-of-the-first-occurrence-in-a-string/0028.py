# Topic : Two Pointers, String, String Matching
class Solution:
    def strStr(self, haystack: str, needle: str) -> int:

        for i in range(len(haystack)):
            if haystack[i] == needle[0]:
                if haystack[i : (i + len(needle))] == needle:
                    return i

        return -1


if __name__ == "__main__":
    # input
    # output
    haystack = "sadbutsad"
    needle = "sad"
    print(Solution().strStr(haystack=haystack, needle=needle))

    haystack = "leetcode"
    needle = "leeto"
    print(Solution().strStr(haystack=haystack, needle=needle))
