# Topic : Array, Two Pointers
from typing import List


class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:

        k = 0

        for i in range(len(nums)):
            if k < 2 or nums[i] != nums[k - 2]:
                nums[k] = nums[i]
                k = k + 1

        return k


if __name__ == "__main__":

    nums = [1, 1, 1, 2, 2, 3]
    print(Solution().removeDuplicates(nums))

    nums = [0, 0, 1, 1, 1, 1, 2, 3, 3]
    print(Solution().removeDuplicates(nums))
