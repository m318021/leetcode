# Topic : Array, Matrix, Simulation
from typing import List


class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:

        result = list()

        if not matrix:
            return result

        row_start = 0
        row_end = len(matrix[0]) - 1
        col_start = 0
        col_end = len(matrix) - 1

        while row_start <= row_end and col_start <= col_end:

            for i in range(row_start, row_end + 1):
                result.append(matrix[col_start][i])

            for j in range(col_start + 1, col_end + 1):
                result.append(matrix[j][row_end])

            if row_start < row_end and col_start < col_end:
                for i in range(row_end - 1, row_start, -1):
                    result.append(matrix[col_end][i])

                for j in range(col_end, col_start, -1):
                    result.append(matrix[j][row_start])

            row_start += 1
            col_start += 1
            row_end -= 1
            col_end -= 1

        return result


if __name__ == "__main__":
    matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    output = Solution().spiralOrder(matrix)
    print(output)

    matrix = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
    output = Solution().spiralOrder(matrix)
    print(output)

    matrix = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]
    output = Solution().spiralOrder(matrix)
    print(output)
