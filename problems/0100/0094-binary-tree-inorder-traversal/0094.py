# Topic : Stack, Tree, Depth-First Search, Binary Tree
from typing import List
from typing import Optional
from resources.utils.tree_lib import Tree
from resources.utils.tree_lib import TreeNode


class Solution:
    def inorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        res = []

        def inOrder(node):
            if not node:
                return
            inOrder(node.left)
            res.append(node.val)
            inOrder(node.right)

        inOrder(root)
        return res


if __name__ == "__main__":
    root = [1, None, 2, 3]
    tree_01 = Tree().build_binary_tree(root)
    Tree().print_binary_tree(tree_01)
    output = Solution().inorderTraversal(tree_01)
    print(output)
    print("\n")

    root = []
    tree_01 = Tree().build_binary_tree(root)
    Tree().print_binary_tree(tree_01)
    output = Solution().inorderTraversal(tree_01)
    print(output)
    print("\n")

    root = [1]
    tree_01 = Tree().build_binary_tree(root)
    Tree().print_binary_tree(tree_01)
    output = Solution().inorderTraversal(tree_01)
    print(output)
    print("\n")
