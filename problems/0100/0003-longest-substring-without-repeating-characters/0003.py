# Topic : Hash Table, String, Sliding Window


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        table = {}
        result = 0
        start = -1

        for i in range(len(s)):
            if s[i] in table and table[s[i]] > start:
                start = table[s[i]]
            else:
                result = max(result, i - start)
            table[s[i]] = i
        return result


if __name__ == "__main__":
    s = "abcabcbb"
    output = Solution().lengthOfLongestSubstring(s)
    print(output)

    s = "bbbbb"
    output = Solution().lengthOfLongestSubstring(s)
    print(output)

    s = "pwwkew"
    output = Solution().lengthOfLongestSubstring(s)
    print(output)

    s = "aabaab!bb"
    output = Solution().lengthOfLongestSubstring(s)
    print(output)

    s = "tmmzuxt"
    output = Solution().lengthOfLongestSubstring(s)
    print(output)
