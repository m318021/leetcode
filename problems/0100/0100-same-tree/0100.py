# Topic : Tree, Depth-First Search, Breadth-First Search, Binary Tree
import collections
from typing import Optional
from resources.utils.tree_lib import TreeNode


class Solution:
    def isSameTree(self, p: Optional[TreeNode], q: Optional[TreeNode]) -> bool:
        def build_tree(root):
            if root is None:
                return []

            _tree = collections.deque()
            _tree.append([root])
            result = [[root.val]]

            while _tree:
                node_list = _tree.pop()
                temp, result_value = [], []

                for node in node_list:
                    if node.left is not None:
                        temp.append(node.left)
                        result_value.append(node.left.val)
                    else:
                        result_value.append(-10001)
                    if node.right is not None:
                        temp.append(node.right)
                        result_value.append(node.right.val)
                    else:
                        result_value.append(-10001)
                if temp != []:
                    _tree.append(temp)
                    result.append(result_value)

            return result

        tree_p = build_tree(p)
        tree_q = build_tree(q)

        return tree_p == tree_q
