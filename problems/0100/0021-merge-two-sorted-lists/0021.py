# Topic : Linked List, Recursion
from typing import Optional
from resources.utils.link_list_lib import LinkList
from resources.helpers.leetcode_lib import ListNode

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next


class Solution:
    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:

        result = ListNode(-1)
        head = result

        while list1 and list2:
            if list1.val <= list2.val:
                head.next = list1
                list1 = list1.next
            else:
                head.next = list2
                list2 = list2.next

            head = head.next

        head.next = list1 if list2 is None else list2

        return result.next


if __name__ == "__main__":
    list1 = [1, 2, 4]
    list2 = [1, 3, 4]
    l1 = LinkList().build_list(list1)
    l2 = LinkList().build_list(list2)
    output = Solution().mergeTwoLists(l1, l2)
    LinkList().print_list(output)

    list1 = []
    list2 = [0]
    l1 = LinkList().build_list(list1)
    l2 = LinkList().build_list(list2)
    output = Solution().mergeTwoLists(l1, l2)
    LinkList().print_list(output)
