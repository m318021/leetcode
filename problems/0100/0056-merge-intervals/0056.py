# Topic : Array, Sorting
from typing import List


class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:

        intervals.sort()
        ans = [intervals[0]]

        for s, e in intervals[1:]:

            if ans[-1][1] < s:
                ans.append([s, e])
            else:
                ans[-1][1] = max(ans[-1][1], e)

        return ans


if __name__ == "__main__":

    intervals = [[1, 3], [2, 6], [8, 10], [15, 18]]
    expect = [[1, 6], [8, 10], [15, 18]]
    output = Solution().merge(intervals)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    intervals = [[1, 4], [4, 5]]
    expect = [[1, 5]]
    output = Solution().merge(intervals)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    intervals = [[1, 4], [0, 0]]
    expect = [[0, 0], [1, 4]]
    output = Solution().merge(intervals)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    intervals = [[1, 4], [1, 5]]
    expect = [[1, 5]]
    output = Solution().merge(intervals)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
