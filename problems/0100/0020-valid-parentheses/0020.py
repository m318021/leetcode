# Topic : String, Stack
class Solution:
    def isValid(self, s: str) -> bool:

        stack = []
        table = {"(": ")", "{": "}", "[": "]"}

        for i in range(len(s)):
            if s[i] in table:
                stack.append(s[i])
            else:
                if len(stack) <= 0 or s[i] != table[stack.pop()]:
                    return False

        return len(stack) == 0


if __name__ == "__main__":
    s = "()"
    result = Solution().isValid(s)
    print("Input: {}, Output = {}\nExpect = True\n".format(s, result))

    s = "()[]{}"
    result = Solution().isValid(s)
    print("Input: {}, Output = {}\nExpect = True\n".format(s, result))

    s = "(]"
    result = Solution().isValid(s)
    print("Input: {}, Output = {}\nExpect = False\n".format(s, result))

    s = "([)]"
    result = Solution().isValid(s)
    print("Input: {}, Output = {}\nExpect = False\n".format(s, result))

    s = "{[]}"
    result = Solution().isValid(s)
    print("Input: {}, Output = {}\nExpect = True\n".format(s, result))
