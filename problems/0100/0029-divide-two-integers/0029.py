# Topic : Math, Bit Manipulation


class Solution:
    def divide(self, dividend: int, divisor: int) -> int:

        if dividend % divisor == 0:
            result = dividend // divisor
        else:
            result = int(dividend / divisor)

        if result < (-(2**31)):
            return -(2**31)
        elif result > (2**31 - 1):
            return 2**31 - 1

        return result


if __name__ == "__main__":

    dividend = 10
    divisor = 3
    expect = 3
    output = Solution().divide(dividend, divisor)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    dividend = 7
    divisor = -3
    expect = -2
    output = Solution().divide(dividend, divisor)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
