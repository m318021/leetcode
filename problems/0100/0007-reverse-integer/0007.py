# Topic : Math
class Solution:
    def reverse(self, x: int) -> int:

        value = -x if x < 0 else x

        result = 0

        while value > 0:
            temp = value % 10
            result = result * 10 + temp
            value = value // 10

        if result > 2**31 - 1:
            return 0

        result = -result if x < 0 else result

        return result


if __name__ == "__main__":
    x = 123
    print(Solution().reverse(x))

    x = -123
    print(Solution().reverse(x))

    x = 120
    print(Solution().reverse(x))
