# Topic : Math, Dynamic Programming, Memoization


class Solution:
    def climbStairs(self, n: int) -> int:
        def _climbStairs(n, climb):
            if n == 0 or n == 1:
                return 1
            if n not in climb:
                climb[n] = _climbStairs(n - 1, climb) + _climbStairs(n - 2, climb)

            return climb[n]

        climb = {}
        return _climbStairs(n, climb)


if __name__ == "__main__":
    n = 2
    output = Solution().climbStairs(n)
    print(output)

    n = 3
    output = Solution().climbStairs(n)
    print(output)

    n = 10
    output = Solution().climbStairs(n)
    print(output)
