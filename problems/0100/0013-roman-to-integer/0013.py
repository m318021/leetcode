# Topic : Hash Table, Math, String


class Solution:
    def romanToInt(self, s: str) -> int:
        roman_num = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}

        pre = roman_num[s[0]]

        i = 1
        result = 0

        while i <= len(s):

            if i == len(s):
                value = 0
            else:
                value = roman_num[s[i]]
            result = result + pre if pre >= value else result - pre
            pre = value

            i = i + 1

        return result


if __name__ == "__main__":
    s = "III"
    output = Solution().romanToInt(s)
    print(output)

    s = "LVIII"
    output = Solution().romanToInt(s)
    print(output)

    s = "MCMXCIV"
    output = Solution().romanToInt(s)
    print(output)
