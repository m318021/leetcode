# Topic : Array, String
from typing import List


class Solution:
    def numberOfLines(self, widths: List[int], s: str) -> List[int]:

        i, line, pixels = 0, 1, 0

        while i < len(s):
            pixels += widths[ord(s[i]) - ord("a")]
            if pixels > 100:
                line += 1
                i = i - 1
                pixels = 0
            i = i + 1

        return [line, pixels]


if __name__ == "__main__":

    widths = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
    s = "abcdefghijklmnopqrstuvwxyz"
    expect = [3, 60]
    output = Solution().numberOfLines(widths, s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    widths = [4, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
    s = "bbbcccdddaaa"
    expect = [2, 4]
    output = Solution().numberOfLines(widths, s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    widths = [3, 4, 10, 4, 8, 7, 3, 3, 4, 9, 8, 2, 9, 6, 2, 8, 4, 9, 9, 10, 2, 4, 9, 10, 8, 2]
    s = "mqblbtpvicqhbrejb"
    expect = [1, 100]
    output = Solution().numberOfLines(widths, s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
