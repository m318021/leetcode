# Topic : Array, Hash Table, String
from typing import List


class Solution:
    def uniqueMorseRepresentations(self, words: List[str]) -> int:

        map = [
            ".-",
            "-...",
            "-.-.",
            "-..",
            ".",
            "..-.",
            "--.",
            "....",
            "..",
            ".---",
            "-.-",
            ".-..",
            "--",
            "-.",
            "---",
            ".--.",
            "--.-",
            ".-.",
            "...",
            "-",
            "..-",
            "...-",
            ".--",
            "-..-",
            "-.--",
            "--..",
        ]

        result = []
        for str in words:
            temp = ""
            for j in str:
                temp += map[ord(j) - 97]
            if temp not in result:
                result.append(temp)

        return len(result)

        return 0


if __name__ == "__main__":

    words = ["gin", "zen", "gig", "msg"]
    expect = 2
    output = Solution().uniqueMorseRepresentations(words)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    words = ["a"]
    expect = 1
    output = Solution().uniqueMorseRepresentations(words)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
