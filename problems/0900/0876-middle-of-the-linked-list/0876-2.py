# Topic : Hash Table, Linked List, Two Pointers
from typing import Optional
from resources.utils.link_list_lib import LinkList
from resources.helpers.leetcode_lib import ListNode

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next


class Solution:
    def middleNode(self, head: Optional[ListNode]) -> Optional[ListNode]:
        fast = head
        slow = head
        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next
        return slow


if __name__ == "__main__":
    head = [1, 2, 3, 4, 5]
    list1 = LinkList().build_list(head)
    print("Input:")
    LinkList().print_list(list1)
    output = Solution().middleNode(list1)
    print("Output")
    LinkList().print_list(output)
    print("\n")

    head = [1, 2, 3, 4, 5, 6]
    list1 = LinkList().build_list(head)
    print("Input:")
    LinkList().print_list(list1)
    output = Solution().middleNode(list1)
    print("Output")
    LinkList().print_list(output)
    print("\n")
