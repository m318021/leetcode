# Topic : Two Pointers, String, Stack, Simulation


class Solution:
    def backspaceCompare(self, s: str, t: str) -> bool:
        def stack(test_string):
            out_list = []
            for i in range(len(test_string)):
                if test_string[i] != "#":
                    out_list.append(test_string[i])
                else:
                    if len(out_list) > 0:
                        out_list.pop()
            return out_list

        stack_s = stack(s)
        stack_t = stack(t)

        return stack_s == stack_t


if __name__ == "__main__":
    s = "ab#c"
    t = "ad#c"
    output = Solution().backspaceCompare(s, t)
    print(output)

    s = "ab##"
    t = "c#d#"
    output = Solution().backspaceCompare(s, t)
    print(output)

    s = "a#c"
    t = "b"
    output = Solution().backspaceCompare(s, t)
    print(output)

    s = "###a#c"
    t = "b"
    output = Solution().backspaceCompare(s, t)
    print(output)
