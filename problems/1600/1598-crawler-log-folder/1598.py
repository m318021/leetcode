# Topic : Array, String, Stack
from typing import List


class Solution:
    def minOperations(self, logs: List[str]) -> int:

        level = 0
        for cmd in logs:
            if cmd == "../":
                level -= 1
                level = 0 if level < 0 else level
            elif cmd == "./":
                continue
            else:
                level += 1

        return level


if __name__ == "__main__":

    logs = ["d1/", "d2/", "../", "d21/", "./"]
    expect = 2
    output = Solution().minOperations(logs)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    logs = ["d1/", "d2/", "./", "d3/", "../", "d31/"]
    expect = 3
    output = Solution().minOperations(logs)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    logs = ["d1/", "../", "../", "../"]
    expect = 0
    output = Solution().minOperations(logs)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    logs = ["d1/", "d1/", "../", "d21/", "./"]
    expect = 2
    output = Solution().minOperations(logs)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    logs = ["d1/", "d1/", "d1/", "../", "d21/", "../", "./"]
    expect = 2
    output = Solution().minOperations(logs)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
