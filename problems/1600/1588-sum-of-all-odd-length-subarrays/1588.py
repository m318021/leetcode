# Topic : Array, Math, Prefix Sum
from typing import List


class Solution:
    def sumOddLengthSubarrays(self, arr: List[int]) -> int:

        ans = 0

        for i in range(1, len(arr) + 1, 2):
            _sum = sum(arr[:i])
            ans += _sum
            for j in range(i, len(arr)):
                _sum += arr[j] - arr[j - i]
                ans += _sum

        return ans


if __name__ == "__main__":

    arr = [1, 4, 2, 5, 3]
    expect = 58
    output = Solution().sumOddLengthSubarrays(arr)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    arr = [1, 2]
    expect = 3
    output = Solution().sumOddLengthSubarrays(arr)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    arr = [10, 11, 12]
    expect = 66
    output = Solution().sumOddLengthSubarrays(arr)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
