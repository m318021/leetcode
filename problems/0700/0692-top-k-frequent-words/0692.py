# Topic : Hash Table, String, Trie, Sorting, Heap (Priority Queue), Bucket Sort, Counting
from typing import List


class Solution:
    def topKFrequent(self, words: List[str], k: int) -> List[str]:

        freq = {}

        for word in words:
            freq[word] = freq.get(word, 0) + 1

        freq_list = [(word, freq) for word, freq in freq.items()]

        freq_list.sort(key=lambda x: (-x[1], x[0]))

        return [t[0] for t in freq_list[:k]]


if __name__ == "__main__":
    words = ["i", "love", "leetcode", "i", "love", "coding"]
    k = 2
    output = Solution().topKFrequent(words, k)
    print(output)

    words = ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"]
    k = 4
    output = Solution().topKFrequent(words, k)
    print(output)
