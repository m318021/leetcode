# Topic : Array, Stack, Simulation
from typing import List


class Solution:
    def calPoints(self, operations: List[str]) -> int:

        stack = []

        for i in range(len(operations)):
            if operations[i] not in "CD+":
                stack.append(int(operations[i]))
            else:
                if operations[i] == "C":
                    if len(stack) > 0:
                        stack.pop()
                if operations[i] == "D":
                    if len(stack) > 0:
                        value = stack[-1]
                        stack.append(value * 2)
                if operations[i] == "+":
                    value_01 = stack[-1]
                    value_02 = stack[-2]
                    value = value_01 + value_02
                    stack.append(value)

        return sum(stack)


if __name__ == "__main__":

    ops = ["5", "2", "C", "D", "+"]
    expect = 30
    output = Solution().calPoints(ops)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    ops = ["5", "-2", "4", "C", "D", "9", "+", "+"]
    expect = 27
    output = Solution().calPoints(ops)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    ops = ["1", "C"]
    expect = 0
    output = Solution().calPoints(ops)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
