# Topic : Array, Math, Sorting
from typing import List


class Solution:
    def maximumProduct(self, nums: List[int]) -> int:

        nums.sort()

        head, tail = -3, -1
        value = []
        while tail < 2:
            temp = 1
            for i in range(head, tail + 1):
                temp = temp * nums[i]
            value.append(temp)
            head += 1
            tail += 1

        return max(value)
