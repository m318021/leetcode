# Topic : Tree, Depth-First Search, Breadth-First Search, Binary Tree
import collections
from typing import List
from typing import Optional
from resources.utils.tree_lib import TreeNode


class Solution:
    def averageOfLevels(self, root: Optional[TreeNode]) -> List[float]:
        tree_queue = collections.deque()
        tree_queue.append([root])
        result = [float(root.val)]

        while tree_queue:
            node_list = tree_queue.popleft()
            temp = []
            temp_result = []

            for node in node_list:
                if node.left is not None:
                    temp.append(node.left)
                    temp_result.append(node.left.val)
                if node.right is not None:
                    temp.append(node.right)
                    temp_result.append(node.right.val)

            if temp != []:
                tree_queue.append(temp)
                _sum = sum(temp_result)
                f_value = _sum / len(temp_result)
                result.append(f_value)

        return result
