# Topic : Array, Hash Table, Bit Manipulation, Sorting
from typing import List


class Solution:
    def findErrorNums(self, nums: List[int]) -> List[int]:

        _nums = [i for i in range(1, len(nums) + 1)]
        set_01 = set(nums)
        set_02 = set(_nums)

        set_miss = set_02 - set_01
        miss = list(set_miss)[0]

        duplicate = sum(nums) - (sum(_nums) - miss)

        return [duplicate, miss]


if __name__ == "__main__":

    nums = [1, 2, 2, 4]
    expect = [2, 3]
    output = Solution().findErrorNums(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [1, 1]
    expect = [1, 2]
    output = Solution().findErrorNums(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
