# Topic : Math, Two Pointers, Binary Search
class Solution:
    def judgeSquareSum(self, c: int) -> bool:

        if c == 0:
            return True

        x = c
        sql = int(x**0.5)
        while sql > 0:

            temp = x - sql * sql
            _sql = int(temp**0.5)
            if temp == _sql * _sql:
                return True
            sql -= 1

        return False


if __name__ == "__main__":
    c = 5
    expect = True
    output = Solution().judgeSquareSum(c)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    c = 3
    expect = False
    output = Solution().judgeSquareSum(c)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    c = 74
    expect = True
    output = Solution().judgeSquareSum(c)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    c = 10
    expect = True
    output = Solution().judgeSquareSum(c)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    c = 1000
    expect = True
    output = Solution().judgeSquareSum(c)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    c = 0
    expect = True
    output = Solution().judgeSquareSum(c)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
