# Topic : Math, Two Pointers, Binary Search
class Solution:
    def judgeSquareSum(self, c: int) -> bool:

        head, tail = 0, int(c**0.5)

        while head <= tail:
            temp = head * head + tail * tail
            if temp == c:
                return True
            elif temp < c:
                head += 1
            else:
                tail -= 1

        return False


if __name__ == "__main__":
    c = 5
    expect = True
    output = Solution().judgeSquareSum(c)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    c = 3
    expect = False
    output = Solution().judgeSquareSum(c)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    c = 74
    expect = True
    output = Solution().judgeSquareSum(c)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    c = 10
    expect = True
    output = Solution().judgeSquareSum(c)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    c = 1000
    expect = True
    output = Solution().judgeSquareSum(c)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    c = 0
    expect = True
    output = Solution().judgeSquareSum(c)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
