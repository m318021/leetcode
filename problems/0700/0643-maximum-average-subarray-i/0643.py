# Topic : Array, Sliding Window
from typing import List


class Solution:
    def findMaxAverage(self, nums: List[int], k: int) -> float:

        start = 0
        value = 0
        result = float("-inf")
        for i in range(len(nums)):
            length = i - start + 1
            value += nums[i]

            if length == k:
                result = max(float(value / k), result)
                value = value - nums[start]
                start += 1

        return result


if __name__ == "__main__":
    nums = [1, 12, -5, -6, 50, 3]
    k = 4
    output = Solution().findMaxAverage(nums, k)
    print(output)

    nums = [5]
    k = 1
    output = Solution().findMaxAverage(nums, k)
    print(output)
