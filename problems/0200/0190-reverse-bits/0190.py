# Topic : Divide and Conquer, Bit Manipulation
class Solution:
    def reverseBits(self, n: int) -> int:

        result = 0

        for i in range(32):

            result = result << 1
            bit = n % 2
            n = n >> 1
            result = result + bit

        return result

        # _n = ""

        # for i in range(32 - len(str(bin(n))[2:])):
        #     _n = _n + "0"
        # _n = _n + str(bin(n))[2:]

        # ans = 0
        # for i in range(len(_n)):
        #     ans = ans + int(_n[i]) * (2**i)

        # return ans


if __name__ == "__main__":
    # n = 0000001010010100000111101001110
    n = 43261596
    _assert = Solution().reverseBits(n) == 964176192
    result = "PASSED" if _assert is True else "FALSED"

    print(f"ans = {Solution().reverseBits(n)}, result = {result}")

    # n = 11111111111111111111111111111101
    n = 3221225471
    _assert = Solution().reverseBits(n) == 4294967293
    result = "PASSED" if _assert is True else "FALSED"

    print(f"ans = {Solution().reverseBits(n)}, result = {result}")
