# Topic : Tree, Breadth-First Search, Binary Tree
import collections
from typing import List
from typing import Optional
from resources.helpers.leetcode_lib import TreeNode


class Solution:
    def zigzagLevelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:

        if root is None:
            return []

        tree_queue = collections.deque()
        tree_queue.append([root])
        result = [[root.val]]
        depth = 0

        while tree_queue:
            node_list = tree_queue.popleft()
            temp = []
            temp_result = []

            for node in node_list:
                if node.left is not None:
                    temp.append(node.left)
                    temp_result.append(node.left.val)
                if node.right is not None:
                    temp.append(node.right)
                    temp_result.append(node.right.val)

            depth += 1
            if temp != []:
                tree_queue.append(temp)
                if depth % 2 == 1:
                    temp_result = reversed(temp_result)
                result.append(temp_result)

        return result
