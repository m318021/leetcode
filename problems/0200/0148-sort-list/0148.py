# Topic : Linked List, Two Pointers, Divide and Conquer, Sorting, Merge Sort
from typing import Optional
from resources.utils.link_list_lib import LinkList
from resources.utils.link_list_lib import ListNode

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next


class Solution:
    def sortList(self, head: Optional[ListNode]) -> Optional[ListNode]:

        if head is None or head.next is None:
            return head

        slow = head
        fast = head.next

        while fast is not None and fast.next is not None:
            slow = slow.next
            fast = fast.next.next

        head2 = slow.next
        slow.next = None

        head = self.sortList(head)
        LinkList().print_list(head)
        head2 = self.sortList(head2)
        LinkList().print_list(head2)

        return self.merge(head, head2)

    def merge(self, a, b):
        dummy = ListNode(-1)
        curr = dummy

        while a is not None and b is not None:
            if a.val <= b.val:
                curr.next = a
                a = a.next
            else:
                curr.next = b
                b = b.next
            curr = curr.next

        if a is not None:
            curr.next = a
        else:
            curr.next = b

        return dummy.next


if __name__ == "__main__":
    nums = [4, 2, 1, 3]
    head = LinkList().build_list(nums)
    print("HEAD : ")
    LinkList().print_list(head)
    output = Solution().sortList(head)
    print("Output : ")
    LinkList().print_list(output)

    nums = [-1, 5, 3, 4, 0]
    head = LinkList().build_list(nums)
    print("HEAD : ")
    LinkList().print_list(head)
    output = Solution().sortList(head)
    print("Output : ")
    LinkList().print_list(output)
