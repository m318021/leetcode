# Topic : Tree, Depth-First Search, Breadth-First Search, Binary Tree
from typing import Optional
from resources.utils.tree_lib import TreeNode
from resources.utils.tree_lib import Tree


class Solution:
    def minDepth(self, root: Optional[TreeNode]) -> int:

        if root is None:
            return 0

        if root.left is None:
            return 1 + self.minDepth(root.right)

        if root.right is None:
            return 1 + self.minDepth(root.left)

        return 1 + min(self.minDepth(root.left), self.minDepth(root.right))


if __name__ == "__main__":

    items = [3, 9, 20, None, None, 15, 7]
    root = Tree().build_binary_tree(items)
    Tree().print_binary_tree(root)
    expect = 2
    output = Solution().minDepth(root)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    items = [2, None, 3, None, 4, None, 5, None, 6]
    root = Tree().build_binary_tree(items)
    Tree().print_binary_tree(root)
    expect = 5
    output = Solution().minDepth(root)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
