# Topic : Array, Dynamic Programming
from typing import List


class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        buy = float("inf")
        profit = 0

        for price in prices:
            if buy > price:
                buy = price
            if profit < price - buy:
                profit = price - buy

        return profit


if __name__ == "__main__":
    prices = [7, 1, 5, 3, 6, 4]
    output = Solution().maxProfit(prices)
    print(output)

    prices = [7, 6, 4, 3, 1]
    output = Solution().maxProfit(prices)
    print(output)
