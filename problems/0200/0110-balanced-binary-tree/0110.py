# Topic : Tree, Depth-First Search, Binary Tree
from typing import Optional
from resources.utils.tree_lib import TreeNode
from resources.utils.tree_lib import Tree

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isBalanced(self, root: Optional[TreeNode]) -> bool:
        if not root:
            return True

        left_depth = self.dfs(root.left)
        right_depth = self.dfs(root.right)

        if abs(left_depth - right_depth) > 1:
            return False

        return self.isBalanced(root.left) and self.isBalanced(root.right)

    def dfs(self, root) -> int:
        if not root:
            return 0

        return 1 + max(self.dfs(root.left), self.dfs(root.right))


if __name__ == "__main__":
    items = [3, 9, 20, None, None, 15, 7]
    root = Tree().build_binary_tree(items)
    Tree().print_binary_tree(root)
    output = Solution().isBalanced(root)
    print(output)

    items = [1, 2, 2, 3, 3, None, None, 4, 4]
    root = Tree().build_binary_tree(items)
    Tree().print_binary_tree(root)
    output = Solution().isBalanced(root)
    print(output)

    items = []
    root = Tree().build_binary_tree(items)
    Tree().print_binary_tree(root)
    output = Solution().isBalanced(root)
    print(output)

    items = [1, 2, 2, 3, None, None, 3, 4, None, None, 4]
    root = Tree().build_binary_tree(items)
    Tree().print_binary_tree(root)
    output = Solution().isBalanced(root)
    print(output)
