# Topic : Tree, Breadth-First Search, Binary Tree

import collections
from typing import List
from typing import Optional
from resources.helpers.leetcode_lib import TreeNode


class Solution:
    def levelOrderBottom(self, root: Optional[TreeNode]) -> List[List[int]]:

        if root is None:
            return []

        tree_queue = collections.deque()
        tree_queue.append([root])
        result = [[root.val]]

        while tree_queue:
            node_list = tree_queue.pop()
            temp = []
            temp_result = []

            for node in node_list:
                if node.left is not None:
                    temp.append(node.left)
                    temp_result.append(node.left.val)

                if node.right is not None:
                    temp.append(node.right)
                    temp_result.append(node.right.val)

            if temp != []:
                tree_queue.append(temp)
                result.append(temp_result)

        return reversed(result)
