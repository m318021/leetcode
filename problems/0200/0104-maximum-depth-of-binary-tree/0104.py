# Topic : Tree, Depth-First Search, Breadth-First Search, Binary Tree
from typing import Optional
from resources.helpers.leetcode_lib import TreeNode


class Solution:
    def maxDepth(self, root: Optional[TreeNode]) -> int:
        if not root:
            return 0

        return self.dfs(root)

    def dfs(self, root):
        if not root:
            return 0

        return max(self.dfs(root.left), self.dfs(root.right)) + 1
