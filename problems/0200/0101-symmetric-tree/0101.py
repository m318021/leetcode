# Topic : Tree, Depth-First Search, Breadth-First Search, Binary Tree
import collections
from typing import Optional
from resources.utils.tree_lib import TreeNode


class Solution:
    def isSymmetric(self, root: Optional[TreeNode]) -> bool:
        def build_tree(root):
            if root is None:
                return []

            _tree = collections.deque()
            _tree.append([root])
            result = [[root.val]]

            while _tree:
                node_list = _tree.pop()
                temp, result_value = [], []

                for node in node_list:
                    if node.left is not None:
                        temp.append(node.left)
                        result_value.append(node.left.val)
                    else:
                        result_value.append(-10001)
                    if node.right is not None:
                        temp.append(node.right)
                        result_value.append(node.right.val)
                    else:
                        result_value.append(-10001)
                if temp != []:
                    _tree.append(temp)
                    result.append(result_value)

            return result

        def reverse_tree(root):
            if not root:
                return None

            temp = root.left
            root.left = root.right
            root.right = temp

            reverse_tree(root.left)
            reverse_tree(root.right)

            return root

        reverse_left = reverse_tree(root.left)
        tree_l = build_tree(reverse_left)
        tree_r = build_tree(root.right)

        return tree_l == tree_r
