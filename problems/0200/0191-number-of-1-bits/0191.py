# Topic : Divide and Conquer, Bit Manipulation


class Solution:
    def hammingWeight(self, n: int) -> int:

        result = 0

        while n > 0:
            if n % 2 == 1:
                result += 1
            n = n >> 1

        return result


if __name__ == "__main__":

    n = 11
    expect = 3
    output = Solution().hammingWeight(n)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    n = 128
    expect = 1
    output = Solution().hammingWeight(n)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    n = 2147483645
    expect = 30
    output = Solution().hammingWeight(n)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
