# Topic : Hash Table, Linked List, Two Pointers
from typing import Optional
from resources.utils.link_list_lib import LinkList
from resources.helpers.leetcode_lib import ListNode

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next


class Solution:
    def detectCycle(self, head: Optional[ListNode]) -> Optional[ListNode]:

        fast = head
        slow = head

        is_cycle = False

        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next

            if slow == fast:
                is_cycle = True
                break

        if is_cycle is False:
            return None

        check = head
        while check != slow:
            slow = slow.next
            check = check.next

        return check


if __name__ == "__main__":
    head = [3, 2, 0, -4]
    pos = 1
    print("Input:")
    list1 = LinkList().build_list(head)
    LinkList().print_list(list1)
    output = Solution().detectCycle(list1)
    print("Output")
    LinkList().print_list(output)
    print("\n")

    head = [1, 2]
    pos = 0
    list1 = LinkList().build_list(head)
    LinkList().print_list(list1)
    output = Solution().detectCycle(list1)
    print("Output")
    LinkList().print_list(output)
    print("\n")

    head = [1]
    pos = -1
    list1 = LinkList().build_list(head)
    LinkList().print_list(list1)
    output = Solution().detectCycle(list1)
    print("Output")
    LinkList().print_list(output)
    print("\n")
