# Topic : Array, Depth-First Search, Breadth-First Search, Union Find, Matrix
from typing import List


class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        if not grid:
            return 0

        island_count = 0

        for row in range(len(grid)):
            for column in range(len(grid[row])):
                if grid[row][column] == "1":
                    self.dfs(grid, row, column)
                    island_count += 1

        return island_count

    def dfs(self, grid, row, column):
        if not self._is_valid(grid, row, column):
            return
        grid[row][column] = "0"
        directions = [
            (row + 1, column),
            (row - 1, column),
            (row, column + 1),
            (row, column - 1),
        ]
        for x, y in directions:
            self.dfs(grid, x, y)

    def _is_valid(self, grid, row, column):
        if row < 0 or column < 0 or row >= len(grid) or column >= len(grid[0]) or grid[row][column] != "1":
            return False
        else:
            return True


if __name__ == "__main__":
    grid = [
        ["1", "1", "1", "1", "0"],
        ["1", "1", "0", "1", "0"],
        ["1", "1", "0", "0", "0"],
        ["0", "0", "0", "0", "0"],
    ]
    output = Solution().numIslands(grid)
    print(output)

    grid = [
        ["1", "1", "0", "0", "0"],
        ["1", "1", "0", "0", "0"],
        ["0", "0", "1", "0", "0"],
        ["0", "0", "0", "1", "1"],
    ]
    output = Solution().numIslands(grid)
    print(output)
