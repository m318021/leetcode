# Topic : Tree, Breadth-First Search, Binary Tree
import collections
from typing import List
from typing import Optional
from resources.utils.tree_lib import Tree
from resources.helpers.leetcode_lib import TreeNode


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def levelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        if root is None:
            return []

        tree_queue = collections.deque()
        tree_queue.append([root])
        result = [[root.val]]

        while tree_queue:
            node_list = tree_queue.popleft()
            temp = []
            result_temp = []
            for node in node_list:
                if node.left is not None:
                    temp.append(node.left)
                    result_temp.append(node.left.val)
                if node.right is not None:
                    temp.append(node.right)
                    result_temp.append(node.right.val)

            if temp != []:
                tree_queue.append(temp)
                result.append(result_temp)

        return result


if __name__ == "__main__":
    root = [3, 9, 20, None, None, 15, 7]
    tree = Tree().build_binary_tree(root)
    Tree().print_binary_tree(tree)

    output = Solution().levelOrder(tree)
    print(output)
    print("\n")

    root = [1]
    tree = Tree().build_binary_tree(root)
    Tree().print_binary_tree(tree)

    output = Solution().levelOrder(tree)
    print(output)
    print("\n")

    root = []
    tree = Tree().build_binary_tree(root)
    Tree().print_binary_tree(tree)

    output = Solution().levelOrder(tree)
    print(output)
    print("\n")
