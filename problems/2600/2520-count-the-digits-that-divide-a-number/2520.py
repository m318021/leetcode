# Topic : Math
class Solution:
    def countDigits(self, num: int) -> int:

        n, count = num, 0

        while n > 0:
            div = n % 10

            if num % div == 0:
                count += 1
            n = n // 10

        return count


if __name__ == "__main__":

    num = 7
    expect = 1
    output = Solution().countDigits(num)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    num = 121
    expect = 2
    output = Solution().countDigits(num)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    num = 1248
    expect = 4
    output = Solution().countDigits(num)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
