# Topic : Array, Hash Table, String, Backtracking
from typing import List


class Solution:
    def findDifferentBinaryString(self, nums: List[str]) -> str:

        _num = 2 ** len(nums)

        def trans_to_bin(num, length):
            _bin = ""
            count = 0
            while count < length:
                value = num % 2
                num = num // 2
                _bin = _bin + str(value)
                count += 1

            return _bin

        for i in range(_num):
            _bin = trans_to_bin(i, len(nums))
            if len(_bin) < len(nums):
                _zero = ""
                for i in range(len(nums) - len(_bin)):
                    _zero = _zero + "0"
                _bin = _zero + _bin

            if _bin not in nums:
                return _bin

        return ""


if __name__ == "__main__":

    nums = ["01", "10"]
    expect = "00"
    output = Solution().findDifferentBinaryString(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = ["00", "01"]
    expect = "10"
    output = Solution().findDifferentBinaryString(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = ["111", "011", "001"]
    expect = "000"
    output = Solution().findDifferentBinaryString(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
