# Topic : Array, Simulation
from typing import List


class Solution:
    def getConcatenation(self, nums: List[int]) -> List[int]:

        length = len(nums)
        i = 0

        while i < length:
            nums.append(nums[i])
            i += 1

        return nums


if __name__ == "__main__":

    nums = [1, 2, 1]
    expect = [1, 2, 1, 1, 2, 1]
    output = Solution().getConcatenation(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [1, 3, 2, 1]
    expect = [1, 3, 2, 1, 1, 3, 2, 1]
    output = Solution().getConcatenation(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
