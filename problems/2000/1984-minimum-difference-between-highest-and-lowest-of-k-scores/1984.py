# Topic : Array, Sliding Window, Sorting
from typing import List


class Solution:
    def minimumDifference(self, nums: List[int], k: int) -> int:

        nums.sort()
        result = []
        for i in range(len(nums) - k + 1):
            temp = nums[i + k - 1] - nums[i]
            result.append(temp)

        return min(result)

        # method 2
        # nums.sort()
        # start = 0
        # result = float("inf")

        # while start <= len(nums) - k:
        #     value = nums[start + k - 1] - nums[start]
        #     result = min(value, result)

        #     start += 1

        # return result


if __name__ == "__main__":
    nums = [90]
    k = 1
    output = Solution().minimumDifference(nums, k)
    print(output)

    nums = [9, 4, 1, 7]
    k = 2
    output = Solution().minimumDifference(nums, k)
    print(output)

    nums = [9, 4, 1, 7]
    k = 3
    output = Solution().minimumDifference(nums, k)
    print(output)
