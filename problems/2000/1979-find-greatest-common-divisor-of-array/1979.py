# Topic : Array, Math, Number Theory
from typing import List


class Solution:
    def findGCD(self, nums: List[int]) -> int:
        nums.sort()
        _min = nums[0]
        _max = nums[-1]

        for i in range(_min, 0, -1):
            if _max % i == 0 and _min % i == 0:
                return i
