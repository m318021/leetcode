# Topic : Array, Hash Table, String
from typing import List


class Solution:
    def findRestaurant(self, list1: List[str], list2: List[str]) -> List[str]:

        _list = list1 + list2
        index = {}
        map = {}

        len_01 = len(list1)
        for i in range(len(_list)):
            if _list[i] not in map:
                map[_list[i]] = i
            else:
                index[_list[i]] = map[_list[i]] + i - len_01

        min_index = min(index.values())

        result = []

        for key in index:
            if index[key] == min_index:
                result.append(key)

        return result


if __name__ == "__main__":

    list1 = ["Shogun", "Tapioca Express", "Burger King", "KFC"]
    list2 = ["Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun"]
    expect = ["Shogun"]
    output = Solution().findRestaurant(list1, list2)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    list1 = ["Shogun", "Tapioca Express", "Burger King", "KFC"]
    list2 = ["KFC", "Shogun", "Burger King"]
    expect = ["Shogun"]
    output = Solution().findRestaurant(list1, list2)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    list1 = ["happy", "sad", "good"]
    list2 = ["sad", "happy", "good"]
    expect = ["sad", "happy"]
    output = Solution().findRestaurant(list1, list2)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
