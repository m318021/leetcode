# Topic : Array, Greedy, Sorting, Counting Sort
from typing import List


class Solution:
    def arrayPairSum(self, nums: List[int]) -> int:

        nums.sort()
        ans = 0
        for i in range(0, len(nums), 2):
            ans += nums[i]

        return ans


if __name__ == "__main__":

    nums = [1, 4, 3, 2]
    expect = 4
    output = Solution().arrayPairSum(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [6, 2, 6, 5, 1, 2]
    expect = 9
    output = Solution().arrayPairSum(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
