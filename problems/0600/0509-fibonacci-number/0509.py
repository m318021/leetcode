# Topic : Math, Dynamic Programming, Recursion, Memoization
class Solution:
    def fib(self, n: int) -> int:
        fib_table = {0: 0, 1: 1}

        for i in range(2, n + 1):
            fib_table[i] = fib_table[i - 1] + fib_table[i - 2]

        return fib_table[n]


if __name__ == "__main__":
    n = 2
    output = Solution().fib(n)
    print(output)

    n = 3
    output = Solution().fib(n)
    print(output)

    n = 4
    output = Solution().fib(n)
    print(output)
