# Topic : Stack, Tree, Depth-First Search
from typing import List

"""
# Definition for a Node.
"""


class Node:
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children


class Solution:
    def preorder(self, root: "Node") -> List[int]:

        result = []

        def dfs(root):
            if root is None:
                return

            result.append(root.val)
            for node in root.children:
                dfs(node)

        dfs(root)

        return result


if __name__ == "__main__":
    root = [1, None, 3, 2, 4, None, 5, 6]
    Node_1 = Node(1)
    Node_2 = Node(3)
    Node_3 = Node(2)
    Node_4 = Node(4)
    Node_5 = Node(5)
    Node_6 = Node(6)
    Node_1.children = [Node_2, Node_3, Node_4]
    Node_2.children = [Node_5, Node_6]

    output = Solution().preorder(Node_1)
    print(output)

    root = [1, None, 2, 3, 4, 5, None, None, 6, 7, None, 8, None, 9, 10, None, None, 11, None, 12, None, 13, None, None, 14]
