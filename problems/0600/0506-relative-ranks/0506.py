# Topic : Array, Sorting, Heap (Priority Queue)
from typing import List


class Solution:
    def findRelativeRanks(self, score: List[int]) -> List[str]:

        map = {}
        result = ["" for i in range(len(score))]
        print(result)
        for i in range(len(score)):
            map[score[i]] = i

        score.sort()
        for i in range(len(score)):
            if len(score) - i == 1:
                temp = "Gold Medal"
            elif len(score) - i == 2:
                temp = "Silver Medal"
            elif len(score) - i == 3:
                temp = "Bronze Medal"
            else:
                temp = str(len(score) - i)
            result[map[score[i]]] = temp

        return result


if __name__ == "__main__":

    score = [5, 4, 3, 2, 1]
    expect = ["Gold Medal", "Silver Medal", "Bronze Medal", "4", "5"]
    output = Solution().findRelativeRanks(score)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    score = [10, 3, 8, 9, 4]
    expect = ["Gold Medal", "5", "Bronze Medal", "Silver Medal", "4"]
    output = Solution().findRelativeRanks(score)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
