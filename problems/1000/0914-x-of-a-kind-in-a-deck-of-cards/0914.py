# Topic : Array, Hash Table, Math, Counting, Number Theory
from typing import List


class Solution:
    def hasGroupsSizeX(self, deck: List[int]) -> bool:

        temp = {}

        def gcd(a, b):
            _min = a if b > a else b
            _max = b if _min == a else a

            for i in range(_min, 0, -1):
                if _max % i == 0 and _min % i == 0:
                    return i
            return 1

        for num in deck:
            if num not in temp:
                temp[num] = 1
            else:
                temp[num] += 1

        for key in temp:
            if temp[key] < 2:
                return False

        return True
