# Topic : Hash Table, String


class Solution:
    def findPermutationDifference(self, s: str, t: str) -> int:

        hash = {}
        diff = 0
        for i in range(len(s)):
            if s[i] not in hash:
                hash[s[i]] = i
            else:
                diff += abs(hash[s[i]] - i)

            if t[i] not in hash:
                hash[t[i]] = i
            else:
                diff += abs(hash[t[i]] - i)

        return diff


if __name__ == "__main__":
    s = "abc"
    t = "bac"
    print(Solution().findPermutationDifference(s, t))

    s = "abcde"
    t = "edbac"
    print(Solution().findPermutationDifference(s, t))
