# Topic : Array, Hash Table, Prefix Sum
from typing import List
from collections import defaultdict


class Solution:
    def distance(self, nums: List[int]) -> List[int]:

        d = defaultdict(list)
        for i, x in enumerate(nums):
            d[x].append(i)
        dis = [0 for i in range(len(nums))]

        for list_i in d.values():
            left = 0
            right = sum(list_i) - len(list_i) * list_i[0]
            for i in range(len(list_i)):
                dis[list_i[i]] = left + right
                if i + 1 < len(list_i):
                    left += (list_i[i + 1] - list_i[i]) * (i + 1)
                    right -= (list_i[i + 1] - list_i[i]) * (len(list_i) - i - 1)

        return dis


if __name__ == "__main__":
    nums = [1, 3, 1, 1, 2]
    print(Solution().distance(nums))

    nums = [0, 5, 3]
    print(Solution().distance(nums))
