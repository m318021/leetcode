# Topic : Array, Hash Table, Prefix Sum
from typing import List


class Solution:
    def numberOfPoints(self, nums: List[List[int]]) -> int:

        cars = [0] * 101
        for a, b in nums:
            for i in range(a, b + 1):
                cars[i] = 1

        return sum(cars)


if __name__ == "__main__":

    nums = [[3, 6], [1, 5], [4, 7]]
    expect = 7
    output = Solution().numberOfPoints(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [[1, 3], [5, 8]]
    expect = 7
    output = Solution().numberOfPoints(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [[2, 5], [3, 8], [1, 6], [4, 10]]
    expect = 10
    output = Solution().numberOfPoints(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
