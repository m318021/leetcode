# Topic : Hash Table, String


class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False

        table_s = {}
        table_t = {}
        for i in range(len(s)):
            if s[i] not in table_s:
                table_s[s[i]] = t[i]
            else:
                if table_s[s[i]] != t[i]:
                    return False

            if t[i] not in table_t:
                table_t[t[i]] = s[i]
            else:
                if table_t[t[i]] != s[i]:
                    return False

        return True


if __name__ == "__main__":
    s = "egg"
    t = "add"
    output = Solution().isIsomorphic(s, t)
    print(output)

    s = "foo"
    t = "bar"
    output = Solution().isIsomorphic(s, t)
    print(output)

    s = "paper"
    t = "title"
    output = Solution().isIsomorphic(s, t)
    print(output)
