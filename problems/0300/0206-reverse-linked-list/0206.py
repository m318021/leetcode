# Topic : Linked List, Recursion
from typing import Optional
from resources.utils.link_list_lib import LinkList
from resources.helpers.leetcode_lib import ListNode

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next


class Solution:
    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:

        pre_node = None

        while head:
            reverse = head.next
            head.next = pre_node
            pre_node = head
            head = reverse

        return pre_node


if __name__ == "__main__":
    head = [1, 2, 3, 4, 5]

    print("input:")
    link_list_01 = LinkList().build_list(head)
    LinkList().print_list(link_list_01)
    print("output")
    output = Solution().reverseList(link_list_01)
    LinkList().print_list(output)
    print("\n")

    head = [1, 2]

    print("input:")
    link_list_01 = LinkList().build_list(head)
    LinkList().print_list(link_list_01)
    print("output")
    output = Solution().reverseList(link_list_01)
    LinkList().print_list(output)
    print("\n")

    head = []

    print("input:")
    link_list_01 = LinkList().build_list(head)
    LinkList().print_list(link_list_01)
    print("output")
    output = Solution().reverseList(link_list_01)
    LinkList().print_list(output)
    print("\n")
