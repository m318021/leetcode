# Topic : Hash Table, Math, Two Pointers


class Solution:
    def isHappy(self, n: int) -> bool:

        table = []

        def process(num):
            value = 0
            while num > 0:
                temp = num % 10
                value = value + temp * temp
                num = num // 10

            return value

        while n > 0:
            n = process(n)
            if n not in table:
                table.append(n)
            else:
                break

        return n == 1


if __name__ == "__main__":
    n = 19
    output = Solution().isHappy(n)
    print(output)

    n = 2
    output = Solution().isHappy(n)
    print(output)

    n = 1234
    output = Solution().isHappy(n)
    print(output)

    n = 7
    output = Solution().isHappy(n)
    print(output)

    n = 1111111
    output = Solution().isHappy(n)
    print(output)
