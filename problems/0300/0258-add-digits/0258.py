# Topic : Math, Simulation, Number Theory
class Solution:
    def addDigits(self, num: int) -> int:
        def caculate(value):
            ans = 0

            while value > 0:
                temp = value % 10
                ans = ans + temp
                value = value // 10

            return ans

        while True:
            result = caculate(num)

            if result < 10:
                return result
            num = result

        # Method 2
        # if num == 0:
        #     return 0

        # elif num % 9 == 0:
        #     return 9
        # else:
        #     return num % 9


if __name__ == "__main__":

    num = 38
    expect = 2
    output = Solution().addDigits(num)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    num = 0
    expect = 0
    output = Solution().addDigits(num)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
