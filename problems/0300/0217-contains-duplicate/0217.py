# Topic : Array, Hash Table, Sorting
from typing import List


class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:

        # method 1
        # set_nums = set(nums)

        # return len(set_nums) != len(nums)

        repeat = {}

        for i in range(len(nums)):
            if nums[i] not in repeat:
                repeat[nums[i]] = 1
            else:
                return True

        return False


if __name__ == "__main__":

    nums = [1, 2, 3, 1]
    expect = True
    output = Solution().containsDuplicate(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [1, 2, 3, 4]
    expect = False
    output = Solution().containsDuplicate(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [1, 1, 1, 3, 3, 4, 3, 2, 4, 2]
    expect = True
    output = Solution().containsDuplicate(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
