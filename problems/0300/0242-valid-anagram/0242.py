# Topic : Hash Table, String, Sorting


class Solution:
    def isAnagram(self, s: str, t: str) -> bool:

        _s = list(s)
        _t = list(t)

        _s.sort()
        _t.sort()

        return _s == _t

        """ Method 2"""
        # table_s = {}
        # table_t = {}

        # if len(s) != len(t):
        #     return False

        # for i in range(len(s)):
        #     if s[i] not in table_s:
        #         table_s[s[i]] = 0
        #     else:
        #         table_s[s[i]] += 1

        #     if t[i] not in table_t:
        #         table_t[t[i]] = 0
        #     else:
        #         table_t[t[i]] += 1

        # if len(table_s) != len(table_t):
        #     return False
        # for key in table_s:
        #     if key not in table_t:
        #         return False
        #     else:
        #         if table_s[key] != table_t[key]:
        #             return False
        # return True


if __name__ == "__main__":

    s = "anagram"
    t = "nagaram"
    expect = True
    output = Solution().isAnagram(s, t)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "rat"
    t = "car"
    expect = False
    output = Solution().isAnagram(s, t)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
