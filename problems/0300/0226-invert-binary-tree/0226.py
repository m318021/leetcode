# Topic : Tree, Depth-First Search, Breadth-First Search, Binary Tree
from typing import Optional
from resources.utils.tree_lib import TreeNode
from resources.utils.tree_lib import Tree

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def invertTree(self, root: Optional[TreeNode]) -> Optional[TreeNode]:

        if not root:
            return None

        temp = root.left
        root.left = root.right
        root.right = temp

        self.invertTree(root.left)
        self.invertTree(root.right)

        return root


if __name__ == "__main__":
    items = [4, 2, 7, 1, 3, 6, 9]
    root = Tree().build_binary_tree(items)
    Tree().print_binary_tree(root)
    output = Solution().invertTree(root)
    Tree().print_binary_tree(output)

    items = [2, 1, 3]
    root = Tree().build_binary_tree(items)
    Tree().print_binary_tree(root)
    output = Solution().invertTree(root)
    Tree().print_binary_tree(output)

    items = []
    root = Tree().build_binary_tree(items)
    Tree().print_binary_tree(root)
    output = Solution().invertTree(root)
    Tree().print_binary_tree(output)
