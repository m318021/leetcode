# Topic : Math, Bit Manipulation, Recursion
class Solution:
    def isPowerOfTwo(self, n: int) -> bool:

        result = 1

        for i in range(32):
            if result == n:
                return True
            elif result > n:
                return False
            result = result << 1

        return False


if __name__ == "__main__":

    n = 1
    expect = True
    output = Solution().isPowerOfTwo(n)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    n = 16
    expect = True
    output = Solution().isPowerOfTwo(n)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    n = 3
    expect = False
    output = Solution().isPowerOfTwo(n)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
