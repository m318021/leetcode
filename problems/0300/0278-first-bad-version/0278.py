# Topic : Binary Search, Interactive
def isBadVersion(n, bad=4):
    if n == bad:
        return True
    else:
        return False


class Solution:
    def firstBadVersion(self, n: int) -> int:
        if n == 1 and isBadVersion(n):
            return 1

        start, end = 1, n

        while start <= end:
            mid = (start + end) // 2
            if isBadVersion(mid):
                if isBadVersion(mid - 1) is False:
                    return mid
                else:
                    end = mid - 1
            else:
                start = mid + 1

        return 0


if __name__ == "__main__":
    n = 5
    bad = 4
    output = Solution().firstBadVersion(n)
    print(output)

    n = 1
    bad = 1
    output = Solution().firstBadVersion(n)
    print(output)
