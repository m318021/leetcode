# Topic : Linked List, Two Pointers, Stack, Recursion
from typing import Optional
from resources.utils.link_list_lib import LinkList
from resources.utils.link_list_lib import ListNode


class Solution:
    def isPalindrome(self, head: Optional[ListNode]) -> bool:

        if head is None or head.next is None:
            return True

        slow = head
        fast = head.next

        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next

        cur = slow.next
        slow.next = None
        pre_node = None

        while cur is not None:
            reverse = cur.next
            cur.next = pre_node
            pre_node = reverse
            cur = reverse

        while pre_node is not None and head is not None:
            if pre_node.val != head.val:
                return False
            pre_node = pre_node.next
            head = head.next

        return True


if __name__ == "__main__":
    head = [1, 2, 2, 1]
    list_01 = LinkList().build_list(head)
    LinkList().print_list(list_01)
    output = Solution().isPalindrome(list_01)
    print(output)

    head = [1, 2]
    list_01 = LinkList().build_list(head)
    LinkList().print_list(list_01)
    output = Solution().isPalindrome(list_01)
    print(output)
