# Topic : Hash Table, String, Counting


class Solution:
    def getHint(self, secret: str, guess: str) -> str:

        bulls = 0
        cows = 0
        secret_count = [0 for i in range(10)]
        guess_count = [0 for i in range(10)]

        for i in range(len(secret)):
            s = secret[i]
            g = guess[i]
            if s == g:
                bulls += 1
            else:
                secret_count[int(s)] += 1
                guess_count[int(g)] += 1

        for i in range(10):
            cows += min(secret_count[i], guess_count[i])

        return f"{bulls}A{cows}B"


if __name__ == "__main__":
    secret = "1807"
    guess = "7810"
    output = Solution().getHint(secret, guess)
    print(output)

    secret = "1123"
    guess = "0111"
    output = Solution().getHint(secret, guess)
    print(output)

    secret = "11"
    guess = "10"
    output = Solution().getHint(secret, guess)
    print(output)

    secret = "011"
    guess = "110"
    output = Solution().getHint(secret, guess)
    print(output)

    secret = "11"
    guess = "01"
    output = Solution().getHint(secret, guess)
    print(output)

    secret = "1122"
    guess = "1222"
    output = Solution().getHint(secret, guess)
    print(output)
