# Topic : Hash Table, String


class Solution:
    def wordPattern(self, pattern: str, s: str) -> bool:
        _s = s.split(" ")
        map = {}

        if len(set(pattern)) != len(set(_s)):
            return False

        if len(pattern) != len(_s):
            return False

        for i in range(len(pattern)):

            if pattern[i] not in map:
                map[pattern[i]] = _s[i]
            else:
                if map[pattern[i]] != _s[i]:
                    return False

        return True


if __name__ == "__main__":

    pattern = "abba"
    s = "dog cat cat dog"
    expect = True
    output = Solution().wordPattern(pattern, s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    pattern = "abba"
    s = "dog cat cat fish"
    expect = False
    output = Solution().wordPattern(pattern, s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    pattern = "aaaa"
    s = "dog cat cat dog"
    expect = False
    output = Solution().wordPattern(pattern, s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    pattern = "abba"
    s = "dog dog dog dog"
    expect = False
    output = Solution().wordPattern(pattern, s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
