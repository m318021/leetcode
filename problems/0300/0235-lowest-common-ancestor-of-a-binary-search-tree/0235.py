# Topic : Tree, Depth-First Search, Binary Search Tree, Binary Tree
from resources.utils.tree_lib import Tree
from resources.utils.tree_lib import TreeNode

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


class Solution:
    def lowestCommonAncestor(self, root: "TreeNode", p: "TreeNode", q: "TreeNode") -> "TreeNode":

        result = root

        while result:
            if result.val < p.val and result.val < q.val:
                result = result.right
            elif result.val > p.val and result.val > q.val:
                result = result.left
            else:
                return result


if __name__ == "__main__":
    root = [6, 2, 8, 0, 4, 7, 9, None, None, 3, 5]
    p = 2
    q = 8

    bst = Tree().build_binary_tree(root)
    Tree().print_binary_tree(bst)
    output = Solution().lowestCommonAncestor(bst, TreeNode(p), TreeNode(q))
    print(output.val)
    print("\n")

    root = [6, 2, 8, 0, 4, 7, 9, None, None, 3, 5]
    p = 2
    q = 4

    bst = Tree().build_binary_tree(root)
    Tree().print_binary_tree(bst)
    output = Solution().lowestCommonAncestor(bst, TreeNode(p), TreeNode(q))
    print(output.val)
    print("\n")
