# Topic : Array, Hash Table, Sliding Window
from typing import List


class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:

        table = {}

        for i in range(len(nums)):
            if nums[i] not in table:
                table[nums[i]] = i
            else:
                s = table[nums[i]]
                table[nums[i]] = i
                distance = i - s
                if distance <= k:
                    return True

        return False


if __name__ == "__main__":
    nums = [1, 2, 3, 1]
    k = 3
    output = Solution().containsNearbyDuplicate(nums, k)
    print(output)

    nums = [1, 0, 1, 1]
    k = 1
    output = Solution().containsNearbyDuplicate(nums, k)
    print(output)

    nums = [1, 2, 3, 1, 2, 3]
    k = 2
    output = Solution().containsNearbyDuplicate(nums, k)
    print(output)

    nums = [4, 1, 2, 3, 1, 5]
    k = 3
    output = Solution().containsNearbyDuplicate(nums, k)
    print(output)

    nums = [0, 1, 2, 3, 4, 0, 0, 7, 8, 9, 10, 11, 12, 0]
    k = 1
    output = Solution().containsNearbyDuplicate(nums, k)
    print(output)
