# Topic : String
class Solution:
    def areNumbersAscending(self, s: str) -> bool:
        _s = s.split(" ")
        current = 0
        for sub in _s:
            result = 0
            if sub[0] in "0123456789":
                for ch in sub:
                    result = result * 10 + int(ch)

                if result > current:
                    current = result
                else:
                    return False

        return True

        """ method 2 """
        # _s = s.split(" ")
        # before, current = -1, 0

        # for st in _s:
        #     current = 0
        #     check = False
        #     for ch in st:
        #         if ch in "0123456789":
        #             check = True
        #             value = ord(ch) - ord("0")
        #             current = current * 10 + value
        #     if check is True:
        #         if current > before:
        #             before = current
        #         else:
        #             return False

        # return True


if __name__ == "__main__":
    s = "1 box has 3 blue 4 red 6 green and 12 yellow marbles"
    expect = True
    output = Solution().areNumbersAscending(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "hello world 5 x 5"
    expect = False
    output = Solution().areNumbersAscending(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "sunset is at 7 51 pm overnight lows will be in the low 50 and 60 s"
    expect = False
    output = Solution().areNumbersAscending(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
