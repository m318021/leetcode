# Topic : Array, Prefix Sum
from typing import List


class Solution:
    def pivotIndex(self, nums: List[int]) -> int:

        for i in range(1, len(nums)):
            nums[i] = nums[i] + nums[i - 1]

        result = -1
        for i in range(len(nums)):
            if i == 0:
                if nums[len(nums) - 1] - nums[i] == 0:
                    return 0
            else:
                if nums[len(nums) - 1] - nums[i] == nums[i - 1]:
                    return i

        return result


if __name__ == "__main__":

    nums = [1, 7, 3, 6, 5, 6]
    output = Solution().pivotIndex(nums)
    print(output)

    nums = [1, 2, 3]
    output = Solution().pivotIndex(nums)
    print(output)

    nums = [2, 1, -1]
    output = Solution().pivotIndex(nums)
    print(output)
