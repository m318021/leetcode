# Topic : Array, Depth-First Search, Breadth-First Search, Matrix
from typing import List


class Solution:
    def floodFill(self, image: List[List[int]], sr: int, sc: int, color: int) -> List[List[int]]:
        length_r, length_c = len(image), len(image[0])
        cur_color = image[sr][sc]
        if cur_color == color:
            return image

        def dfs(r, c):
            if image[r][c] == cur_color:
                image[r][c] = color

                if r >= 1:
                    dfs(r - 1, c)

                if r + 1 < length_r:
                    dfs(r + 1, c)

                if c >= 1:
                    dfs(r, c - 1)

                if c + 1 < length_c:
                    dfs(r, c + 1)

        dfs(sr, sc)

        return image


if __name__ == "__main__":
    image = [[1, 1, 1], [1, 1, 0], [1, 0, 1]]
    sr = 1
    sc = 1
    color = 2
    output = Solution().floodFill(image, sr, sc, color)
    print(output)

    image = [[0, 0, 0], [0, 0, 0]]
    sr = 0
    sc = 0
    color = 0
    output = Solution().floodFill(image, sr, sc, color)
    print(output)
