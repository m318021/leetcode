# Topic : Array, Dynamic Programming
from typing import List


class Solution:
    def minCostClimbingStairs(self, cost: List[int]) -> int:

        n = len(cost)

        cost.append(0)
        result = [0 for i in range(n + 1)]

        result[0], result[1] = cost[0], cost[1]

        for i in range(2, n + 1):
            result[i] = min(result[i - 1], result[i - 2]) + cost[i]

        return result[n]


if __name__ == "__main__":
    cost = [10, 15, 20]
    output = Solution().minCostClimbingStairs(cost)
    print(output)

    cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1]
    output = Solution().minCostClimbingStairs(cost)
    print(output)
