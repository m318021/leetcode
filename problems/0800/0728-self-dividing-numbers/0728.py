# Topic : Math
from typing import List


class Solution:
    def selfDividingNumbers(self, left: int, right: int) -> List[int]:
        def countDigits(num):

            n = num

            while n > 0:
                div = n % 10
                if div == 0:
                    return False

                if num % div != 0:
                    return False
                n = n // 10

            return True

        result = []
        for i in range(left, right + 1):
            if countDigits(i) is True:
                result.append(i)

        return result


if __name__ == "__main__":

    left = 1
    right = 22
    expect = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22]
    output = Solution().selfDividingNumbers(left, right)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    left = 47
    right = 85
    expect = [48, 55, 66, 77]
    output = Solution().selfDividingNumbers(left, right)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
