# Topic : Array
from typing import List


class Solution:
    def check(self, nums: List[int]) -> bool:

        _min = nums[0]
        temp = []
        start = 0
        for i in range(len(nums)):
            if nums[i] >= _min:
                _min = nums[i]
                temp.append(nums[i])
            else:
                start = i
                break

        result = nums[start:] + nums[:start]
        nums.sort()
        return result == nums


if __name__ == "__main__":

    nums = [3, 4, 5, 1, 2]
    expect = True
    output = Solution().check(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [2, 1, 3, 4]
    expect = False
    output = Solution().check(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [1, 2, 3]
    expect = True
    output = Solution().check(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [5, 5, 6, 6, 6, 9, 1, 2]
    expect = True
    output = Solution().check(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [6, 7, 7, 5]
    expect = True
    output = Solution().check(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [2, 4, 1, 3]
    expect = False
    output = Solution().check(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    nums = [10, 1, 4, 5, 10]
    expect = True
    output = Solution().check(nums)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
