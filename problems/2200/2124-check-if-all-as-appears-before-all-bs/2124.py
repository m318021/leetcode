# Topic : String
class Solution:
    def checkString(self, s: str) -> bool:

        current = "a"

        for i in range(len(s)):
            if s[i] == "b":
                current = "b"
            if s[i] != current:
                return False

        return True

        """ method 2 """
        # _s = s.split(" ")
        # current = 0
        # for sub in _s:
        #     result = 0
        #     if sub[0] in "0123456789":
        #         for ch in sub:
        #             result = result * 10 + int(ch)

        #         if result > current:
        #             current = result
        #         else:
        #             return False

        # return True


if __name__ == "__main__":

    s = "aaabbb"
    expect = True
    output = Solution().checkString(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "abab"
    expect = True
    output = Solution().checkString(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    s = "bbb"
    expect = True
    output = Solution().checkString(s)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
