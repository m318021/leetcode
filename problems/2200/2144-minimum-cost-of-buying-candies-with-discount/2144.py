# Topic : Array, Greedy, Sorting
from typing import List


class Solution:
    def minimumCost(self, cost: List[int]) -> int:

        if len(cost) < 3:
            return sum(cost)

        cost.sort()
        index = 0
        _cost = 0

        for i in reversed(range(len(cost))):
            if index % 3 != 2:
                _cost += cost[i]
            index += 1

        return _cost


if __name__ == "__main__":

    cost = [1, 2, 3]
    expect = 5
    output = Solution().minimumCost(cost)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    cost = [6, 5, 7, 9, 2, 2]
    expect = 23
    output = Solution().minimumCost(cost)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    cost = [5, 5]
    expect = 10
    output = Solution().minimumCost(cost)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    cost = [3, 3, 3, 1]
    expect = 7
    output = Solution().minimumCost(cost)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")

    cost = [10, 5, 9, 4, 1, 9, 10, 2, 10, 8]
    expect = 48
    output = Solution().minimumCost(cost)
    result = "PASSED" if expect == output else "FAILED"
    print(f"output= {output}, expect = {expect}, result = {result}")
