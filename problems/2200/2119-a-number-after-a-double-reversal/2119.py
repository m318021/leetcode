# Topic : Math
class Solution:
    def isSameAfterReversals(self, num: int) -> bool:

        if num < 10:
            return True

        if num % 10 == 0:
            return False
        else:
            return True


if __name__ == "__main__":
    num = 526
    print(Solution().isSameAfterReversals(num))

    num = 1800
    print(Solution().isSameAfterReversals(num))

    num = 0
    print(Solution().isSameAfterReversals(num))
