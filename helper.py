import os
import sys
import getopt
import logging
from resources.tools.add_folders import AddProblemFolder
from resources.tools.formatter import formatter_cmd
from resources.tools.git_process import git_push_cmd
from resources.tools.update_readme import UpdateReadMe

logging.basicConfig(level=logging.INFO)

argument_list = sys.argv[1:]

options = "hg:ufa:"
long_option = [
    "help",
    "git_push:",
    "update_readme",
    "formatter",
    "add_cases:",
]

arguments, value = getopt.getopt(argument_list, options, long_option)

try:
    for current_argument, current_value in arguments:
        if current_argument in ("-a", "--add_cases"):

            str_cases_list = current_value
            case_list = str_cases_list.split(",")

            logging.info("Add Problems")
            AddProblemFolder().func_add_problem_folders(case_list)

        elif current_argument in ("-u", "--update_readme"):

            logging.info("Update README.md")
            UpdateReadMe().func_update_read_me()

        elif current_argument in ("-f", "--formatter"):

            cmd = formatter_cmd()

            # black
            logging.info(" -- Run black command")
            logging.info(f"# {cmd['black_cmd']}")
            os.system(cmd["black_cmd"])

            # run flake8
            logging.info(" -- Run flake8 command")
            logging.info(f"# {cmd['flake8_cmd']}")
            os.system(cmd["flake8_cmd"])

        elif current_argument in ("-g", "--git_push"):
            git_push_cmd(current_value)

        elif current_argument in ("-h", "--help"):
            logging.info("[-f HELP][-a ADD CASES 'case_id_1, case_id_2, ...'][-u UPDATE README][-f FORMATTER][-g GIT PUSH 'message']")

except getopt.error as err:
    logging.info(str(err))
